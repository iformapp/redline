package com.redline.coin.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.redline.coin.R;
import com.redline.coin.adapter.SignalAdapter;
import com.redline.coin.model.Signals;
import com.redline.coin.util.Constants;
import com.redline.coin.util.Preferences;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.ceryle.segmentedbutton.SegmentedButton;
import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignalActivity extends BaseActivity {

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.rv_signals)
    RecyclerView rvSignals;
    @BindView(R.id.tab_open)
    SegmentedButton tabOpen;
    @BindView(R.id.tab_close)
    SegmentedButton tabClose;
    @BindView(R.id.segmentGroup)
    SegmentedButtonGroup segmentGroup;
    @BindView(R.id.ll_subscription)
    LinearLayout llSubscription;

    private SignalAdapter mAdapter;
    private List<Signals.Data> signalsList;
    private int signal = 2;  // open = 2, close = 3
    private static final int OPEN = 0;
    private static final int CLOSE = 1;
    private boolean isRefresh = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signal);
        ButterKnife.bind(this);

        rvSignals.setLayoutManager(new LinearLayoutManager(this));

        showView();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getSignalByStatus();
            }
        });

        segmentGroup.setOnPositionChangedListener(new SegmentedButtonGroup.OnPositionChangedListener() {
            @Override
            public void onPositionChanged(int position) {
                if (position == OPEN) {
                    signal = Constants.OPEN_SIGNAL;
                    tabOpen.setTypeface(Constants.SFTEXT_BOLD);
                    tabClose.setTypeface(Constants.SFTEXT_REGULAR);
                    showView();
                } else if (position == CLOSE) {
                    signal = Constants.CLOSE_SIGNAL;
                    tabOpen.setTypeface(Constants.SFTEXT_REGULAR);
                    tabClose.setTypeface(Constants.SFTEXT_BOLD);
                    getSignalByStatus();
                }
            }
        });
    }

    public void showView() {
        if (isSignalSubscribed()) { // Check Subscription
            getSignalByStatus();
        } else {
            llSubscription.setVisibility(View.VISIBLE);
        }
    }

    public void getSignalByStatus() {
        if (!isNetworkConnected()) {
            if (swipeRefreshLayout != null)
                swipeRefreshLayout.setRefreshing(false);
            return;
        }

        if (swipeRefreshLayout != null && !swipeRefreshLayout.isRefreshing())
            showProgress();

        Call<Signals> call = getService().getSignalsByStatus(signal, getAccessToken());
        call.enqueue(new Callback<Signals>() {
            @Override
            public void onResponse(Call<Signals> call, Response<Signals> response) {
                Signals signals = response.body();
                if (signals != null) {
                    if (checkStatus(signals)) {
                        signalsList = new ArrayList<>();
                        signalsList = signals.data;
                    } else {
                        isRefresh = true;
                        signalsList = new ArrayList<>();
                    }
                    setAdapter();
                }

                llSubscription.setVisibility(View.GONE);
                hideProgress();
                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<Signals> call, Throwable t) {
                failureError("get signal data failed");
                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    public void setAdapter() {
        if (signalsList != null && signalsList.size() > 0) {
            if (mAdapter == null) {
                mAdapter = new SignalAdapter(this);
            }

            mAdapter.doRefresh(signalsList);

            if (rvSignals != null && rvSignals.getAdapter() == null) {
                rvSignals.setAdapter(mAdapter);
            }
        } else {
            if (mAdapter != null)
                mAdapter.doRefresh(null);
        }
    }

    @OnClick({R.id.img_info, R.id.btn_subscribing})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_info:
                showInfoDialog(0, getDialogInfoData());
                break;
            case R.id.btn_subscribing:
                Intent i = new Intent(this, SubscriptionActivity.class);
                i.putExtra(Constants.INVESTING_SUBSCRIPTION, false);
                startActivity(i);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        String signalId = Preferences.readString(this, Constants.SIGNAL_ID, "");
        if (!isEmpty(signalId)) {
            isRefresh = true;
            Preferences.writeString(this, Constants.SIGNAL_ID, "");
            Intent i = new Intent(this, AlertDetailsActivity.class);
            i.putExtra(Constants.SIGNAL_ID, signalId);
            startActivity(i);
        }

        if (isRefresh) {
            isRefresh = false;
            showView();
        }
    }
}
