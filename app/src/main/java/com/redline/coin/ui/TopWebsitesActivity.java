package com.redline.coin.ui;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.redline.coin.R;
import com.redline.coin.adapter.DialogListAdapter;
import com.redline.coin.adapter.TopWebsiteAdapter;
import com.redline.coin.adapter.TypesAdapter;
import com.redline.coin.model.Coin;
import com.redline.coin.model.CoinInfoModel;
import com.redline.coin.model.LikeDislike;
import com.redline.coin.model.TopWebsite;
import com.redline.coin.model.TopWebsite.Data;
import com.redline.coin.util.Constants;
import com.redline.coin.util.Utils;
import com.redline.coin.util.textview.TextViewSFTextRegular;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TopWebsitesActivity extends BaseActivity {

    @BindView(R.id.tv_back_text)
    TextViewSFTextRegular tvBackText;
    @BindView(R.id.tv_buy_type)
    TextViewSFTextRegular tvBuyType;
    @BindView(R.id.rl_buy_type)
    RelativeLayout rlBuyType;
    @BindView(R.id.tv_currency)
    TextViewSFTextRegular tvCurrency;
    @BindView(R.id.tv_location)
    TextViewSFTextRegular tvLocation;
    @BindView(R.id.rv_topwebsites)
    RecyclerView rvTopwebsites;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    private TypesAdapter typesAdapter;
    private ArrayList<String> coinList;
    private ArrayList<String> locationList;
    private List<Data> topList;
    private TopWebsiteAdapter mAdapter;
    private String[] typesArray;
    private int selectedPosition = 0; // 0 - buy , 1 - exchange
    private boolean isRefresh = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_websites);
        ButterKnife.bind(this);

        rvTopwebsites.setLayoutManager(new LinearLayoutManager(this));

        tvBackText.setText(getString(R.string.more));
        getCoinValue();

        locationList = new ArrayList<>();
        locationList.add("United State - USA");
        locationList.add("United Kingdom - UK");
        locationList.add("India - IND");
        locationList.add("Barbados - BRB");
        locationList.add("Mauritania - MRT");
        locationList.add("Saudi Arabia - KSA");
        locationList.add("Canada - CA");
        locationList.add("Yemen - YEM");

        String[] split = locationList.get(0).split("-");
        tvLocation.setText(split[1].trim());

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getWebsites();
            }
        });
    }

    public String getLocation() {
        return tvLocation.getText().toString();
    }

    public String getCurrency() {
        return tvCurrency.getText().toString();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isRefresh) {
            isRefresh = false;
            getCoinValue();
        }
    }

    public void getWebsites() {
        if (!isNetworkConnected()) {
            if (swipeRefreshLayout != null)
                swipeRefreshLayout.setRefreshing(false);
            return;
        }

        if (swipeRefreshLayout != null && !swipeRefreshLayout.isRefreshing())
            showProgress();

        Call<TopWebsite> call = getService().getTopWebsite(getLocation(), getCurrency(), getTerm(), "admin", getAccessToken());
        call.enqueue(new Callback<TopWebsite>() {
            @Override
            public void onResponse(Call<TopWebsite> call, Response<TopWebsite> response) {
                TopWebsite topWebsite = response.body();
                if (topWebsite != null) {
                    if (checkStatus(topWebsite)) {
                        topList = new ArrayList<>();
                        topList = topWebsite.data;
                    } else {
                        topList = new ArrayList<>();
                    }
                    setAdapter();
                }

                hideProgress();
                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<TopWebsite> call, Throwable t) {
                failureError("get website failed");
                hideProgress();
                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    public void setAdapter() {
        if (topList != null && topList.size() > 0) {
            if (mAdapter == null) {
                mAdapter = new TopWebsiteAdapter(this);
            }

            mAdapter.doRefresh(topList);

            if (rvTopwebsites != null && rvTopwebsites.getAdapter() == null) {
                rvTopwebsites.setAdapter(mAdapter);
            }
        } else {
            if (mAdapter != null)
                mAdapter.doRefresh(null);
        }
    }

    public void getCoinValue() {
        if (!isNetworkConnected()) {
            return;
        }

        showProgress();

        Call<Coin> call = getService().getAllCoin(getAccessToken());
        call.enqueue(new Callback<Coin>() {
            @Override
            public void onResponse(Call<Coin> call, Response<Coin> response) {
                Coin coin = response.body();
                if (coin != null) {
                    if (checkStatus(coin)) {
                        coinList = new ArrayList<>();
                        for (int i = 0; i < coin.data.size(); i++) {
                            Coin.Data data = coin.data.get(i);
                            coinList.add(data.name + " - " + data.webId);
                        }

                        tvCurrency.setText("BTC");
                        getWebsites();
                    } else {
                        isRefresh = true;
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<Coin> call, Throwable t) {
                failureError("get coin failed");
                hideProgress();
            }
        });
    }

    public void onItemClick(int position) {
        openWebsiteDialog(position);
    }

    public void openWebsiteDialog(final int position) {

        final Dialog dialog = new Dialog(this);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_open_website);
        dialog.setCancelable(true);

        TextView tvTitle = dialog.findViewById(R.id.tv_title);
        Button btnYes = dialog.findViewById(R.id.btn_yes);
        Button btnNo = dialog.findViewById(R.id.btn_no);
        final TextView tvLike = dialog.findViewById(R.id.tv_like);
        final TextView tvDislike = dialog.findViewById(R.id.tv_dislike);
        LinearLayout llLike = dialog.findViewById(R.id.ll_like);
        LinearLayout llDislike = dialog.findViewById(R.id.ll_dislike);

        tvTitle.setText(getString(R.string.open_website, topList.get(position).name));
        tvLike.setText(topList.get(position).likes + "");
        tvDislike.setText(topList.get(position).dislikes + "");

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                redirectUsingCustomTab(topList.get(position).url);
            }
        });

        llLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!topList.get(position).uidConfirmation.equals("1")) {
                    topList.get(position).uidConfirmation = "1";
                    likeDislikeStatus(1, position, tvLike, tvDislike); // 1 for like
                }
            }
        });

        llDislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!topList.get(position).uidConfirmation.equals("2")) {
                    topList.get(position).uidConfirmation = "2";
                    likeDislikeStatus(2, position, tvLike, tvDislike); // 2 for dislike
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.gravity = Gravity.CENTER;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void likeDislikeStatus(int likeDislike, final int position, final TextView tvLike, final TextView tvDislike) {
        if (!isNetworkConnected()) {
            return;
        }

        showProgress();

        Call<LikeDislike> call = getService().likeDislike(getUserId(), likeDislike, topList.get(position).id);
        call.enqueue(new Callback<LikeDislike>() {
            @Override
            public void onResponse(Call<LikeDislike> call, Response<LikeDislike> response) {
                LikeDislike model = response.body();
                if (model != null && checkStatus(model)) {
                    LikeDislike.Data data = model.data.get(0);
                    topList.get(position).likes = data.likes;
                    topList.get(position).dislikes = data.dislikes;
                    tvLike.setText(data.likes + "");
                    tvDislike.setText(data.dislikes + "");
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<LikeDislike> call, Throwable t) {
                failureError("website status failed");
                hideProgress();
            }
        });
    }

    @OnClick({R.id.ll_back, R.id.img_info, R.id.rl_buy_type, R.id.rl_currency, R.id.rl_location})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.img_info:
                showInfoDialog(0, getDialogInfoData());
                break;
            case R.id.rl_buy_type:
                showTypesDialog();
                break;
            case R.id.rl_currency:
                if (coinList != null && coinList.size() > 0) {
                    showItemSelectDialog(Constants.TYPE_CURRENCY, coinList, getCurrency());
                }
                break;
            case R.id.rl_location:
                if (locationList != null && locationList.size() > 0) {
                    showItemSelectDialog(Constants.TYPE_LOCATION, locationList, getLocation());
                }
                break;
        }
    }

    public void showItemSelectDialog(final String type, ArrayList<String> arrayList, String selectedText) {
        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_item_select);
        dialog.setCancelable(true);

        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
        TextView tvApply = dialog.findViewById(R.id.tv_apply);
        TextView tvHeading = dialog.findViewById(R.id.tv_heading);
        final EditText etSearch = dialog.findViewById(R.id.et_search);
        RecyclerView rvTypes = dialog.findViewById(R.id.rv_items);

        if (type.equals(Constants.TYPE_CURRENCY)) {
            etSearch.setHint(String.format(getString(R.string.search_for), getString(R.string.cryptocurrency)));
            tvHeading.setText(getString(R.string.cryptocurrency));
        } else {
            etSearch.setHint(String.format(getString(R.string.search_for), getString(R.string.location)));
            tvHeading.setText(getString(R.string.location));
        }

        rvTypes.setLayoutManager(new LinearLayoutManager(this));

        typesAdapter = new TypesAdapter(this, arrayList, selectedText);
        rvTypes.setAdapter(typesAdapter);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (typesAdapter != null && typesAdapter.getSelectedItem() != null) {
                    String[] split = typesAdapter.getSelectedItem().split("-");
                    if (type.equals(Constants.TYPE_CURRENCY)) {
                        tvCurrency.setText(split[1].trim());
                    } else {
                        tvLocation.setText(split[1].trim());
                    }
                    getWebsites();
                }
                dialog.dismiss();
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (typesAdapter != null)
                    typesAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                etSearch.post(new Runnable() {
                    @Override
                    public void run() {
                        Utils.openSoftKeyboard(TopWebsitesActivity.this, etSearch);
                    }
                });
            }
        });
        etSearch.requestFocus();
    }

    private int getTerm() {
        return selectedPosition;
    }

    public void showTypesDialog() {
        typesArray = new String[]{ getString(R.string.buy), getString(R.string.exchange_) };

        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_list_select);
        dialog.setCancelable(true);

        final TextView tvTitle = dialog.findViewById(R.id.tv_title);
        RecyclerView rvList = dialog.findViewById(R.id.rv_list);
        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);

        tvTitle.setText(getString(R.string.term));
        rvList.setLayoutManager(new LinearLayoutManager(this));

        DialogListAdapter adapter = new DialogListAdapter(this, Arrays.asList(typesArray), selectedPosition,
                new DialogListAdapter.OnItemSelect() {
                    @Override
                    public void itemSelect(int position) {
                        selectedPosition = position;
                        tvBuyType.setText(typesArray[position]);
                        dialog.dismiss();
                        getWebsites();
                    }
                });
        rvList.setAdapter(adapter);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public ArrayList<CoinInfoModel> getDialogInfoData() {
        int[] colorList = {R.color.black};
        String[] words = {"Ranks websites based on:"};
        String[] fonts = {Constants.SFTEXT_MEDIUM};

        ArrayList<CoinInfoModel> coinInfoModels = new ArrayList<>();
        coinInfoModels.add(new CoinInfoModel(getString(R.string.top_website_text), fonts, colorList, words));
        return coinInfoModels;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
