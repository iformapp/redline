package com.redline.coin.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.redline.coin.BuildConfig;
import com.redline.coin.R;
import com.redline.coin.util.Utils;
import com.redline.coin.util.textview.TextViewSFTextRegular;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingActivity extends BaseActivity {

    @BindView(R.id.tv_back_text)
    TextViewSFTextRegular tvBackText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);

        tvBackText.setText(getString(R.string.back));
    }

    @OnClick({R.id.ll_back, R.id.ll_share, R.id.ll_rate_us, R.id.ll_contact_us, R.id.ll_hiw, R.id.ll_feedback, R.id.ll_disclaimer})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.ll_share:
                shareApp();
                break;
            case R.id.ll_rate_us:
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                } catch (Exception e) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
                }
                break;
            case R.id.ll_contact_us:
                try {
                    String locale = getResources().getConfiguration().locale.getCountry();
                    String msgBody = "\n \n \n \n --------------- \n Device Type: " + Utils.getDeviceName() +
                            "\n Android Version: " + Build.VERSION.RELEASE + "\n App Version: " + BuildConfig.VERSION_NAME +
                            "\n Country Code: " + locale;
                    Intent sendIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                            "mailto", "info@redlinecoin.com", null));
                    sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback");
                    sendIntent.putExtra(Intent.EXTRA_TEXT, msgBody);
                    startActivity(Intent.createChooser(sendIntent, "Send email..."));
                } catch (Exception e) {
                    Toast.makeText(this, "Mail apps not installed", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.ll_hiw:
                showInfoDialog(0, getDialogInfoData());
                break;
            case R.id.ll_feedback:
                redirectUsingCustomTab("https://www.redlinecoin.com/contact-us.html");
                break;
            case R.id.ll_disclaimer:
                redirectUsingCustomTab("https://www.redlinecoin.com/disclaimer.html");
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
