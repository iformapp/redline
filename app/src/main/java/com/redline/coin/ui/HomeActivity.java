package com.redline.coin.ui;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.redline.coin.R;
import com.redline.coin.adapter.CoinAdapter;
import com.redline.coin.model.Coin;
import com.redline.coin.util.Constants;
import com.redline.coin.util.Preferences;
import com.redline.coin.util.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends BaseActivity implements CoinAdapter.OnTimeChangeListner {

    @BindView(R.id.tv_timer)
    TextView tvTimer;
    @BindView(R.id.rv_coin)
    RecyclerView rvCoin;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    private CoinAdapter mAdapter;
    private Handler handler;
    private List<Coin.Data> coinData;
    private boolean isSecond = false;
    private boolean isRefresh = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        rvCoin.setLayoutManager(new LinearLayoutManager(this));
        getCoinValue();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getCoinValue();
            }
        });
    }

    public void getCoinValue() {
        if (!isNetworkConnected()) {
            if (swipeRefreshLayout != null)
                swipeRefreshLayout.setRefreshing(false);
            return;
        }

        if (swipeRefreshLayout != null && !swipeRefreshLayout.isRefreshing())
            showProgress();

        Call<Coin> call = getService().getCoinValue("1", getAccessToken());
        call.enqueue(new Callback<Coin>() {
            @Override
            public void onResponse(Call<Coin> call, Response<Coin> response) {
                Coin coin = response.body();
                if (coin != null) {
                    if (checkStatus(coin)) {
                        coinData = new ArrayList<>();
                        coinData = coin.data;
                        coinData.add(null);
                        setAdapter();
                    } else {
                        isRefresh = true;
                    }
                }

                hideProgress();
                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<Coin> call, Throwable t) {
                failureError("get coin failed");
                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    public void setAdapter() {
        if (coinData != null && coinData.size() > 0) {
            if (mAdapter == null) {
                mAdapter = new CoinAdapter(HomeActivity.this, this);
            }

            mAdapter.doRefresh(coinData);

            if (rvCoin != null && rvCoin.getAdapter() == null) {
                rvCoin.setAdapter(mAdapter);
            }
        }
    }

    public void startTimer() {
        handler = new Handler();
        handler.postDelayed(runnable, 1000);
    }

    public void stopTimer() {
        if (handler != null) {
            handler.removeCallbacks(runnable);
        }
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (tvTimer != null) {
                tvTimer.setText(Utils.changeDateFormat(isSecond ? "MMM dd, hh:mm:ss a" : "MMM dd, hh:mm a", new Date()));
            }
            handler.postDelayed(this, 1000);
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopTimer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopTimer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startTimer();
        if (isRefresh) {
            isRefresh = false;
            getCoinValue();
        }
        boolean isStatusChange = Preferences.readBoolean(this, Constants.NOTIFICATION_BUYORNOT, false);
        if (isStatusChange) {
            Preferences.writeBoolean(this, Constants.NOTIFICATION_BUYORNOT, false);
            if (swipeRefreshLayout != null)
                swipeRefreshLayout.setRefreshing(true);
            getCoinValue();
        }
    }

    @OnClick({R.id.img_info})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_info:
                showInfoDialog(0, getDialogInfoData());
                break;
        }
    }

    @Override
    public void onTimeChange(boolean isSecond) {
        this.isSecond = isSecond;
    }
}
