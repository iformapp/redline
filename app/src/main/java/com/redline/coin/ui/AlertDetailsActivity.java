package com.redline.coin.ui;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.redline.coin.R;
import com.redline.coin.model.GeneralModel;
import com.redline.coin.model.Signals;
import com.redline.coin.util.Constants;
import com.redline.coin.util.Constants.Currency;
import com.redline.coin.util.Constants.StatusType;
import com.redline.coin.util.Utils;
import com.redline.coin.util.button.ButtonSFTextRegular;
import com.redline.coin.util.textview.TextViewSFTextBold;
import com.redline.coin.util.textview.TextViewSFTextRegular;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.redline.coin.util.Constants.HIGH;
import static com.redline.coin.util.Constants.LOW;
import static com.redline.coin.util.Constants.MEDIUM;

public class AlertDetailsActivity extends BaseActivity {

    @BindView(R.id.tv_whattodo)
    TextViewSFTextRegular tvWhattodo;
    @BindView(R.id.btn_buy_status)
    ButtonSFTextRegular btnBuyStatus;
    @BindView(R.id.tv_price_now)
    TextViewSFTextBold tvPriceNow;
    @BindView(R.id.tv_buy_price)
    TextViewSFTextBold tvBuyPrice;
    @BindView(R.id.tv_stop_loss)
    TextViewSFTextBold tvStopLoss;
    @BindView(R.id.tv_sell_target)
    TextViewSFTextBold tvSellTarget;
    @BindView(R.id.tv_gain)
    TextViewSFTextBold tvGain;
    @BindView(R.id.tv_signal_sent)
    TextViewSFTextBold tvSignalSent;
    @BindView(R.id.tv_reached)
    TextViewSFTextBold tvReached;
    @BindView(R.id.tv_risk)
    TextViewSFTextBold tvRisk;
    @BindView(R.id.tv_exchange)
    TextViewSFTextBold tvExchange;
    @BindView(R.id.tv_close_signal)
    TextViewSFTextRegular tvCloseSignal;
    @BindView(R.id.toggle_close_signal)
    ToggleButton toggleCloseSignal;
    @BindView(R.id.tv_strong_buy)
    TextViewSFTextRegular tvStrongBuy;
    @BindView(R.id.toggle_strong_buy)
    ToggleButton toggleStrongBuy;
    @BindView(R.id.tv_buy)
    TextViewSFTextRegular tvBuy;
    @BindView(R.id.toggle_buy)
    ToggleButton toggleBuy;
    @BindView(R.id.tv_hold)
    TextViewSFTextRegular tvHold;
    @BindView(R.id.toggle_hold)
    ToggleButton toggleHold;
    @BindView(R.id.tv_dont_buy)
    TextViewSFTextRegular tvDontBuy;
    @BindView(R.id.toggle_dont_buy)
    ToggleButton toggleDontBuy;
    @BindView(R.id.tv_strong_dontbuy)
    TextViewSFTextRegular tvStrongDontbuy;
    @BindView(R.id.toggle_strong_dontbuy)
    ToggleButton toggleStrongDontbuy;
    @BindView(R.id.ll_back)
    LinearLayout llBack;
    @BindView(R.id.tv_right)
    TextViewSFTextRegular tvRight;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.img_logo)
    ImageView imgLogo;
    @BindView(R.id.tv_coin_name)
    TextViewSFTextBold tvCoinName;
    @BindView(R.id.tv_remarks)
    TextViewSFTextRegular tvRemarks;
    @BindView(R.id.ll_alert_signal)
    LinearLayout llAlertSignal;

    private String signalId;
    private Signals.Data model;
    private int statusPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_details);
        ButterKnife.bind(this);

        if (getIntent() != null) {
            signalId = getIntent().getStringExtra(Constants.SIGNAL_ID);
        }

        tvRight.setText(getString(R.string.alerts));

        getSignal();
    }

    public void getSignal() {
        if (!isNetworkConnected()) {
            return;
        }

        showProgress();

        Call<Signals> call = getService().getSignal(signalId, getUserId());
        call.enqueue(new Callback<Signals>() {
            @Override
            public void onResponse(Call<Signals> call, Response<Signals> response) {
                Signals signals = response.body();
                if (signals != null) {
                    if (checkStatus(signals)) {
                        model = signals.data.get(0);
                        setUi();
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<Signals> call, Throwable t) {
                failureError("get signal data failed");
            }
        });
    }

    private void setUi() {
        Utils.loadImage(this, Constants.IMAGE_URL + model.imgUrl, imgLogo);
        tvCoinName.setText(model.coinName + " - " + model.webId);
        tvGain.setText(Utils.fromHtml("<big>" + Utils.numberFormatPercentage(model.gainValue) + "%</big>"));
        tvSignalSent.setText(Utils.changeDateFormat("yyyy-MM-dd hh:mm:ss", "MMM dd, yyyy", model.sendTime));
        tvReached.setText(Utils.changeDateFormat("yyyy-MM-dd hh:mm:ss", "MMM dd, yyyy", model.reachTime));
        tvExchange.setText(model.exchangeId);
        tvRemarks.setText(model.remarks);

        tvStrongBuy.setText(Utils.getColorString(this, getString(R.string.strong_buy), "Buy", R.color.light_green));
        tvDontBuy.setText(Utils.getColorString(this, getString(R.string.don_t_buy), "Don't", R.color.light_red));
        tvStrongDontbuy.setText(Utils.getColorString(this, getString(R.string.strong_don_t_buy), "Don't", R.color.light_red));

        toggleCloseSignal.setChecked(model.closeStatus.equals("1"));
        toggleBuy.setChecked(model.buy.equals("1"));
        toggleDontBuy.setChecked(model.dontBuy.equals("1"));
        toggleHold.setChecked(model.hold.equals("1"));
        toggleStrongBuy.setChecked(model.strongBuy.equals("1"));
        toggleStrongDontbuy.setChecked(model.strongDontBuy.equals("1"));

        switch (model.riskType) {
            case LOW:
                tvRisk.setText("Low");
                break;
            case MEDIUM:
                tvRisk.setText("Medium");
                break;
            case HIGH:
                tvRisk.setText("High");
                break;
        }

        if (!TextUtils.isEmpty(model.currency)) {
            setTextAndFont(tvBuyPrice, model.buyTarget, model.currency);
            setTextAndFont(tvPriceNow, model.curruntValue, model.currency);
            setTextAndFont(tvSellTarget, model.sellTarget, model.currency);
            setTextAndFont(tvStopLoss, model.stopLoss, model.currency);
        }

        if (model.status != null && model.status.equals(Constants.CLOSE_STATUS)) {
            tvWhattodo.setText(getString(R.string.status));
            statusPosition = 0;
            setButton(R.drawable.closesignal_button_bg, getString(R.string.signal_closed));
            tvRight.setVisibility(View.GONE);
            llAlertSignal.setVisibility(View.GONE);
        } else if (model.status != null && model.status.equals(Constants.OPEN_STATUS)) {
            tvWhattodo.setText(getString(R.string.what_to_do_now));
            switch (model.statusBuyornot) {
                case "0":
                    statusPosition = Constants.STRONG_DONT_BUY;
                    setButton(R.drawable.red_button_bg, getString(R.string.strong_don_t_buy));
                    break;
                case "1":
                    statusPosition = Constants.DONT_BUY;
                    setButton(R.drawable.red_button_bg, getString(R.string.don_t_buy));
                    break;
                case "2":
                    statusPosition = Constants.HOLD;
                    setButton(R.drawable.blue_button_bg, getString(R.string.hold));
                    break;
                case "3":
                    statusPosition = Constants.BUY;
                    setButton(R.drawable.green_button_bg, getString(R.string.buy));
                    break;
                case "4":
                    statusPosition = Constants.STRONG_BUY;
                    setButton(R.drawable.green_button_bg, getString(R.string.strong_buy));
                    break;
            }
        }

        toggleStrongBuy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                changeAlerts(isChecked ? 1 : 0, StatusType.STRONG_BUY.getStatus());
            }
        });

        toggleBuy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                changeAlerts(isChecked ? 1 : 0, StatusType.BUY.getStatus());
            }
        });

        toggleHold.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                changeAlerts(isChecked ? 1 : 0, StatusType.HOLD.getStatus());
            }
        });

        toggleDontBuy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                changeAlerts(isChecked ? 1 : 0, StatusType.DONT_BUY.getStatus());
            }
        });

        toggleStrongDontbuy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                changeAlerts(isChecked ? 1 : 0, StatusType.STRONG_DONT_BUY.getStatus());
            }
        });

        toggleCloseSignal.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                changeAlerts(isChecked ? 1 : 0, StatusType.CLOSE.getStatus());
            }
        });
    }

    public void changeAlerts(int status, String alertType) {
        if (!isNetworkConnected()) {
            return;
        }

        showProgress();

        Call<GeneralModel> call = getService().changeSignalStatus(status, alertType, signalId, getUserId());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (checkStatus(response.body())) {
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("signal alert failed");
            }
        });
    }

    private void setTextAndFont(TextView textView, String text, String currency) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (c == '0' || c == '.') {
                stringBuilder.append(String.valueOf(c));
            } else {
                break;
            }
        }
        String zeroText = stringBuilder.toString();
        String otherText = text.substring(zeroText.length());
        String htmltext;
        if (currency.equals(Currency.USD.getValue())) {
            htmltext = "<big>" + "$" + text + "</big>";
        } else {
            htmltext = "<small>" + zeroText + "</small><big>" + otherText + " " + currency + "</big> ";
        }
        textView.setText(Utils.fromHtml(htmltext));
    }

    private void setButton(int drawable, String text) {
        btnBuyStatus.setBackground(ContextCompat.getDrawable(this, drawable));
        btnBuyStatus.setText(text);
    }

    @OnClick({R.id.tv_whattodo, R.id.ll_back, R.id.tv_right, R.id.tv_redcoin_site, R.id.tv_status_info})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_whattodo:
                break;
            case R.id.tv_redcoin_site:
                redirectUsingCustomTab(Constants.REDLINE_SITE);
                break;
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.tv_right:
                scrollView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        scrollView.smoothScrollTo(0, scrollView.getBottom());
                    }
                }, 100);
                break;
            case R.id.tv_status_info:
                if (statusPosition != 0)
                    showInfoDialog(statusPosition, getDialogInfoData());
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
