package com.redline.coin.ui;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.redline.coin.R;
import com.redline.coin.fragment.BuyOrNotFragment;
import com.redline.coin.fragment.SignalFragment;
import com.redline.coin.model.CoinInfoModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AlertActivity extends BaseActivity {

    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert);
        ButterKnife.bind(this);

        initTab();
    }

    public void initTab() {
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
    }

    private void setupTabIcons() {
        View view1 = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        TextView tvTabOne = view1.findViewById(R.id.tab);
        tvTabOne.setText(getString(R.string.signal));
        tabLayout.getTabAt(0).setCustomView(view1);

        View view2 = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        TextView tvTabTwo = view2.findViewById(R.id.tab);
        tvTabTwo.setText(getString(R.string.buy_or_not_));
        tabLayout.getTabAt(1).setCustomView(view2);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new SignalFragment(), "");
        adapter.addFrag(new BuyOrNotFragment(), "");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }

    @OnClick({ R.id.img_info })
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_info:
                showInfoDialog(0, getDialogInfoData());
                break;
        }
    }

    @Override
    public ArrayList<CoinInfoModel> getDialogInfoData() {
        int[] colorList = {R.color.light_red, R.color.light_green};
        String[] words = {"Strong Buy", "Strong sell"};

        ArrayList<CoinInfoModel> coinInfoModels = new ArrayList<>();
        coinInfoModels.add(new CoinInfoModel(getString(R.string.notification_info_text), colorList, words));
        return coinInfoModels;
    }
}
