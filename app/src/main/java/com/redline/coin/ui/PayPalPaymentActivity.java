package com.redline.coin.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.redline.coin.R;
import com.redline.coin.util.Constants;
import com.redline.coin.util.textview.TextViewSFDisplayBold;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PayPalPaymentActivity extends BaseActivity {

    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.tv_toolbar_title)
    TextViewSFDisplayBold tvToolbarTitle;

    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        ButterKnife.bind(this);

        if (getIntent() != null) {
            url = getIntent().getStringExtra(Constants.PAYPAL_URL);
        }

        tvToolbarTitle.setText(getString(R.string.payment));

        showProgress();

        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest requestUrl) {
                Log.e("URL", url);
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                Log.e("Page Start URL", url);
                if (url.contains("final_success.php")) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent i = new Intent();
                            setResult(Activity.RESULT_OK, i);
                            finish();
                        }
                    }, 2000);
                } else if (url.contains("cancel.php")) {
                    onBackPressed();
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                hideProgress();
            }
        });

        webView.loadUrl(url);
    }

    @OnClick(R.id.ll_back)
    public void onViewClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
