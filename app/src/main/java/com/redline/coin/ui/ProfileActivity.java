package com.redline.coin.ui;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.redline.coin.R;
import com.redline.coin.model.GeneralModel;
import com.redline.coin.model.UserModel;
import com.redline.coin.util.Constants;
import com.redline.coin.util.Preferences;
import com.redline.coin.util.Utils;
import com.redline.coin.util.textview.TextViewSFDisplayBold;
import com.redline.coin.util.textview.TextViewSFTextRegular;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends BaseActivity {

    @BindView(R.id.tv_toolbar_title)
    TextViewSFDisplayBold tvToolbarTitle;
    @BindView(R.id.tv_username)
    TextViewSFTextRegular tvUsername;
    @BindView(R.id.tv_email)
    TextViewSFTextRegular tvEmail;

    private UserModel.Data userData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        tvToolbarTitle.setText(getString(R.string.profile));

        userData = Preferences.getUserData(this);
        if (userData == null) {
            finish();
            return;
        }

        if (userData.username != null) {
            tvUsername.setText(userData.username);
        }

        if (userData.email != null) {
            tvEmail.setText(userData.email);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }

    @OnClick({R.id.ll_back, R.id.tv_changepassword, R.id.btn_singout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.tv_changepassword:
                redirectActivity(UpdatePasswordActivity.class);
                break;
            case R.id.btn_singout:
                showLogoutDialog();
                break;
        }
    }

    private void showLogoutDialog() {
        final Dialog dialog = new Dialog(this, R.style.Theme_AppCompat_Dialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.setCancelable(true);

        TextView tvMessage = dialog.findViewById(R.id.tv_message);
        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
        TextView tvChatnow = dialog.findViewById(R.id.tv_chat_now);

        String s = getString(R.string.logout_msg);
        String[] words = {"Sign out?"};
        String[] fonts = {Constants.SFTEXT_BOLD};
        tvMessage.setText(Utils.getBoldString(this, s, fonts, null, words));

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvChatnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                logout();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.gravity = Gravity.CENTER;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void logout() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().logout(getUserId());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (response.body() != null) {
                    if (checkStatus(response.body())) {
                        Toast.makeText(ProfileActivity.this, "Sign out successfully", Toast.LENGTH_SHORT).show();
                        Preferences.saveUserData(ProfileActivity.this, null);
                        Preferences.writeBoolean(ProfileActivity.this, Constants.IS_LOGIN, false);
                        Preferences.writeBoolean(ProfileActivity.this, Constants.IS_FIRST, true);

                        Intent i = new Intent(ProfileActivity.this, TutorialActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        openToLeft();
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("logout failed");
            }
        });
    }
}
