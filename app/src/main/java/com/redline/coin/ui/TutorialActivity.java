package com.redline.coin.ui;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.redline.coin.R;
import com.redline.coin.adapter.TutorialPagerAdapter;
import com.redline.coin.util.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class TutorialActivity extends BaseActivity {

    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.indicator)
    CircleIndicator indicator;
    @BindView(R.id.btn_next)
    Button btnNext;

    private TutorialPagerAdapter adapter;
    private boolean isLoginView = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        ButterKnife.bind(this);

        adapter = new TutorialPagerAdapter(this);
        viewPager.setAdapter(adapter);
        indicator.setViewPager(viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == adapter.getCount() - 1) {
                    indicator.setVisibility(View.GONE);
                    btnNext.setVisibility(View.GONE);
                } else {
                    indicator.setVisibility(View.VISIBLE);
                    btnNext.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void changePager() {
        int pos = viewPager.getCurrentItem();
        viewPager.setCurrentItem(pos - 1);
    }

    @OnClick(R.id.btn_next)
    public void onViewClicked() {
        int pos = viewPager.getCurrentItem();
        viewPager.setCurrentItem(pos + 1);
//        if (pos == adapter.getCount() - 1) {
//            isLoginView = true;
//            redirectActivity(LoginSignUpActivity.class);
//        } else {
//            isLoginView = false;
//            viewPager.setCurrentItem(pos + 1);
//        }
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        if (isLoginView && isLogin()) {
//            gotoMainActivity(Constants.TAB_HOME);
//        }
//    }
}
