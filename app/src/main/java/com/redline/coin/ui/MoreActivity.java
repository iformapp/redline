package com.redline.coin.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.redline.coin.BuildConfig;
import com.redline.coin.R;
import com.redline.coin.util.Utils;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MoreActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.rl_share, R.id.rl_contact_us, R.id.rl_setting, R.id.rl_top_websites, R.id.rl_top_youtubers,
            R.id.rl_more_apps, R.id.img_profile})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rl_share:
                shareApp();
                break;
            case R.id.rl_contact_us:
                try {
                    String locale = getResources().getConfiguration().locale.getCountry();
                    String msgBody = "\n \n \n \n --------------- \n Device Type: " + Utils.getDeviceName() +
                            "\n Android Version: " + Build.VERSION.RELEASE + "\n App Version: " + BuildConfig.VERSION_NAME +
                            "\n Country Code: " + locale;
                    Intent sendIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                            "mailto", "info@redlinecoin.com", null));
                    sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback");
                    sendIntent.putExtra(Intent.EXTRA_TEXT, msgBody);
                    startActivity(Intent.createChooser(sendIntent, "Send email..."));
                } catch (Exception e) {
                    Toast.makeText(this, "Mail apps not installed", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.rl_setting:
                redirectActivity(SettingActivity.class);
                break;
            case R.id.rl_top_websites:
                redirectActivity(TopWebsitesActivity.class);
                break;
            case R.id.rl_top_youtubers:
                redirectActivity(TopYouTubeActivity.class);
                break;
            case R.id.rl_more_apps:
                redirectActivity(MoreAppsActivity.class);
                break;
            case R.id.img_profile:
                redirectActivity(ProfileActivity.class);
                break;
        }
    }
}
