package com.redline.coin.ui;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.redline.coin.R;
import com.redline.coin.adapter.DialogListAdapter;
import com.redline.coin.adapter.TypesAdapter;
import com.redline.coin.adapter.YouTubeChannelAdapter;
import com.redline.coin.model.CoinInfoModel;
import com.redline.coin.model.Language;
import com.redline.coin.model.LikeDislike;
import com.redline.coin.model.YouTubeChannel;
import com.redline.coin.model.YouTubeChannel.Data;
import com.redline.coin.util.Constants;
import com.redline.coin.util.Utils;
import com.redline.coin.util.edittext.EditTextSFTextRegular;
import com.redline.coin.util.textview.TextViewSFTextRegular;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TopYouTubeActivity extends BaseActivity {

    @BindView(R.id.tv_back_text)
    TextViewSFTextRegular tvBackText;
    @BindView(R.id.img_search)
    ImageView imgSearch;
    @BindView(R.id.tv_right)
    TextViewSFTextRegular tvRight;
    @BindView(R.id.tv_sort_type)
    TextViewSFTextRegular tvSortType;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.rv_topyoutube)
    RecyclerView rvTopyoutube;
    @BindView(R.id.et_search)
    EditTextSFTextRegular etSearch;

    private TypesAdapter typesAdapter;
    private ArrayList<String> languageList;
    private List<Data> youTubeChannelList;
    private YouTubeChannelAdapter mAdapter;
    private String[] sortArray;
    private int selectedPosition = 0;
    private boolean isSearchMode = false;
    private String selectedLanguage;
    private boolean isRefresh = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_youtube);
        ButterKnife.bind(this);

        rvTopyoutube.setLayoutManager(new LinearLayoutManager(this));

        tvBackText.setText(getString(R.string.more));
        imgSearch.setImageResource(R.drawable.search_red);
        tvRight.setText(getString(R.string.english));

        getLanguages();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getYouTubeChannel();
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void getLanguages() {
        if (!isNetworkConnected()) {
            return;
        }

        showProgress();

        Call<Language> call = getService().getLanguages();
        call.enqueue(new Callback<Language>() {
            @Override
            public void onResponse(Call<Language> call, Response<Language> response) {
                Language language = response.body();
                if (language != null) {
                    if (checkStatus(language)) {
                        languageList = new ArrayList<>();
                        for (int i = 0; i < language.data.size(); i++) {
                            Language.Data data = language.data.get(i);
                            languageList.add(data.name);
                        }

                        if (languageList != null && languageList.size() > 0) {
                            selectedLanguage = languageList.get(0);
                            tvRight.setText(selectedLanguage);
                        }
                    }
                }
                hideProgress();
                getYouTubeChannel();
            }

            @Override
            public void onFailure(Call<Language> call, Throwable t) {
                failureError("get languages failed");
                hideProgress();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isRefresh) {
            isRefresh = false;
            getYouTubeChannel();
        }
    }

    public void getYouTubeChannel() {
        if (!isNetworkConnected()) {
            if (swipeRefreshLayout != null)
                swipeRefreshLayout.setRefreshing(false);
            return;
        }

        if (swipeRefreshLayout != null && !swipeRefreshLayout.isRefreshing())
            showProgress();

        String sortType;
        switch (selectedPosition) {
            case 0:
                sortType = "viewCount";
                break;
            case 1:
                sortType = "likes";
                break;
            case 2:
                sortType = "subscriberCount";
                break;
            case 3:
                sortType = "videoCount";
                break;
            default:
                sortType = "id";
                break;
        }

        Call<YouTubeChannel> call = getService().getYouTubeChannel(selectedLanguage, getAccessToken(), sortType);
        call.enqueue(new Callback<YouTubeChannel>() {
            @Override
            public void onResponse(Call<YouTubeChannel> call, Response<YouTubeChannel> response) {
                YouTubeChannel youTubeChannel = response.body();
                if (youTubeChannel != null) {
                    if (checkStatus(youTubeChannel)) {
                        youTubeChannelList = new ArrayList<>();
                        youTubeChannelList = youTubeChannel.data;
                    } else {
                        isRefresh = true;
                        youTubeChannelList = new ArrayList<>();
                    }
                    setAdapter();
                }

                hideProgress();
                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<YouTubeChannel> call, Throwable t) {
                failureError("get YouTubeChannel failed");
                hideProgress();
                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    public void setAdapter() {
        if (youTubeChannelList != null && youTubeChannelList.size() > 0) {
            if (mAdapter == null) {
                mAdapter = new YouTubeChannelAdapter(this);
            }

            mAdapter.doRefresh(youTubeChannelList);

            if (rvTopyoutube != null && rvTopyoutube.getAdapter() == null) {
                rvTopyoutube.setAdapter(mAdapter);
            }
        } else {
            if (mAdapter != null)
                mAdapter.doRefresh(null);
        }
    }

    @OnClick({R.id.ll_back, R.id.img_info, R.id.img_search, R.id.tv_right, R.id.rl_sort_type})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.img_info:
                showInfoDialog(0, getDialogInfoData());
                break;
            case R.id.img_search:
                isSearchMode = true;
                etSearch.setText("");
                etSearch.setVisibility(View.VISIBLE);
                etSearch.requestFocus();
                Utils.openSoftKeyboard(this, etSearch);
                tvRight.setText(getString(R.string.cancel));
                break;
            case R.id.tv_right:
                if (isSearchMode) {
                    isSearchMode = false;
                    etSearch.setText("");
                    etSearch.setVisibility(View.GONE);
                    tvRight.setText(selectedLanguage);
                } else {
                    showItemSelectDialog(languageList, tvRight.getText().toString());
                }
                break;
            case R.id.rl_sort_type:
                showTypesDialog();
                break;
        }
    }

    public void showItemSelectDialog(ArrayList<String> arrayList, String selectedText) {
        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_item_select);
        dialog.setCancelable(true);

        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
        TextView tvApply = dialog.findViewById(R.id.tv_apply);
        TextView tvHeading = dialog.findViewById(R.id.tv_heading);
        final EditText etSearch = dialog.findViewById(R.id.et_search);
        RecyclerView rvTypes = dialog.findViewById(R.id.rv_items);

        etSearch.setHint(String.format(getString(R.string.search_for), getString(R.string.languages)));
        tvHeading.setVisibility(View.GONE);

        rvTypes.setLayoutManager(new LinearLayoutManager(this));
        typesAdapter = new TypesAdapter(this, arrayList, selectedText);
        rvTypes.setAdapter(typesAdapter);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (typesAdapter != null && typesAdapter.getSelectedItem() != null) {
                    selectedLanguage = typesAdapter.getSelectedItem();
                    tvRight.setText(selectedLanguage);
                    getYouTubeChannel();
                }
                dialog.dismiss();
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (typesAdapter != null)
                    typesAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                etSearch.post(new Runnable() {
                    @Override
                    public void run() {
                        Utils.openSoftKeyboard(TopYouTubeActivity.this, etSearch);
                    }
                });
            }
        });
        etSearch.requestFocus();
    }

    public void showTypesDialog() {
        sortArray = new String[]{getString(R.string.popularity), getString(R.string.rate),
                getString(R.string.subscribers), getString(R.string.uploads), getString(R.string.all)};

        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_list_select);
        dialog.setCancelable(true);

        TextView tvTitle = dialog.findViewById(R.id.tv_title);
        RecyclerView rvList = dialog.findViewById(R.id.rv_list);
        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);

        tvTitle.setText(getString(R.string.sort));
        rvList.setLayoutManager(new LinearLayoutManager(this));

        DialogListAdapter adapter = new DialogListAdapter(this, Arrays.asList(sortArray), selectedPosition,
                new DialogListAdapter.OnItemSelect() {
                    @Override
                    public void itemSelect(int position) {
                        selectedPosition = position;
                        tvSortType.setText(sortArray[position]);
                        dialog.dismiss();
                        getYouTubeChannel();
                    }
                });
        rvList.setAdapter(adapter);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void onItemClick(List<Data> mDatasetFiltered, int position) {
        openWebsiteDialog(mDatasetFiltered, position);
    }

    public void openWebsiteDialog(final List<Data> mDatasetFiltered, final int position) {

        final Dialog dialog = new Dialog(this);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_open_website);
        dialog.setCancelable(true);

        TextView tvTitle = dialog.findViewById(R.id.tv_title);
        Button btnYes = dialog.findViewById(R.id.btn_yes);
        Button btnNo = dialog.findViewById(R.id.btn_no);
        final TextView tvLike = dialog.findViewById(R.id.tv_like);
        final TextView tvDislike = dialog.findViewById(R.id.tv_dislike);
        LinearLayout llLike = dialog.findViewById(R.id.ll_like);
        LinearLayout llDislike = dialog.findViewById(R.id.ll_dislike);

        tvTitle.setText(getString(R.string.open_youtube));
        tvLike.setText(mDatasetFiltered.get(position).likes + "");
        tvDislike.setText(mDatasetFiltered.get(position).dislikes + "");

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                redirectUsingCustomTab("https://www.youtube.com/channel/" + mDatasetFiltered.get(position).channelId);
            }
        });

        llLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mDatasetFiltered.get(position).yidConfirmation.equals("1")) {
                    mDatasetFiltered.get(position).yidConfirmation = "1";
                    likeDislikeStatus(1, position, tvLike, tvDislike, mDatasetFiltered); // 1 for like
                }
            }
        });

        llDislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mDatasetFiltered.get(position).yidConfirmation.equals("2")) {
                    mDatasetFiltered.get(position).yidConfirmation = "2";
                    likeDislikeStatus(2, position, tvLike, tvDislike, mDatasetFiltered); // 2 for dislike
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.gravity = Gravity.CENTER;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void likeDislikeStatus(int likeDislike, final int position, final TextView tvLike,
                                  final TextView tvDislike, final List<Data> mDatasetFiltered) {
        if (!isNetworkConnected()) {
            return;
        }

        showProgress();

        Call<LikeDislike> call = getService().likeDislikeYouTube(getUserId(), likeDislike, mDatasetFiltered.get(position).id);
        call.enqueue(new Callback<LikeDislike>() {
            @Override
            public void onResponse(Call<LikeDislike> call, Response<LikeDislike> response) {
                LikeDislike model = response.body();
                if (model != null && checkStatus(model)) {
                    LikeDislike.Data data = model.data.get(0);
                    mDatasetFiltered.get(position).likes = data.likes;
                    mDatasetFiltered.get(position).dislikes = data.dislikes;
                    tvLike.setText(data.likes + "");
                    tvDislike.setText(data.dislikes + "");
                    mAdapter.refreshFilterList(mDatasetFiltered);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<LikeDislike> call, Throwable t) {
                failureError("website status failed");
                hideProgress();
            }
        });
    }

    @Override
    public ArrayList<CoinInfoModel> getDialogInfoData() {
        int[] colorList = {R.color.black};
        String[] words = {"Ranks websites based on:"};
        String[] fonts = {Constants.SFTEXT_MEDIUM};

        ArrayList<CoinInfoModel> coinInfoModels = new ArrayList<>();
        coinInfoModels.add(new CoinInfoModel(getString(R.string.top_youtube_text), fonts, colorList, words));
        return coinInfoModels;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
