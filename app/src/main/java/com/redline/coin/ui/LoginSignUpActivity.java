package com.redline.coin.ui;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.redline.coin.R;
import com.redline.coin.model.GeneralModel;
import com.redline.coin.model.UserModel;
import com.redline.coin.util.Constants;
import com.redline.coin.util.Preferences;
import com.redline.coin.util.Utils;
import com.redline.coin.util.edittext.EditTextSFTextRegular;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.ceryle.segmentedbutton.SegmentedButton;
import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginSignUpActivity extends BaseActivity {

    private static final int LOGIN = 1;
    private static final int SIGNUP = 0;

    @BindView(R.id.tab_signup)
    SegmentedButton tabSignup;
    @BindView(R.id.tab_login)
    SegmentedButton tabLogin;
    @BindView(R.id.segmentLoginGroup)
    SegmentedButtonGroup segmentLoginGroup;
    @BindView(R.id.et_email)
    EditTextSFTextRegular etEmail;
    @BindView(R.id.et_password)
    EditTextSFTextRegular etPassword;
    @BindView(R.id.ll_login)
    LinearLayout llLogin;
    @BindView(R.id.et_s_email)
    EditTextSFTextRegular etSEmail;
    @BindView(R.id.et_username)
    EditTextSFTextRegular etUsername;
    @BindView(R.id.et_s_password)
    EditTextSFTextRegular etSPassword;
    @BindView(R.id.ll_signup)
    LinearLayout llSignup;

    private boolean isLoginForm = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_sign_up);
        ButterKnife.bind(this);

        if (getIntent() != null) {
            isLoginForm = getIntent().getBooleanExtra(Constants.FROM_LOGIN, false);
        }

        if (isLoginForm) {
            loginVisible();
        } else {
            signupVisible();
        }

        segmentLoginGroup.setOnPositionChangedListener(new SegmentedButtonGroup.OnPositionChangedListener() {
            @Override
            public void onPositionChanged(int position) {
                if (position == LOGIN) {
                    isLoginForm = true;
                    loginVisible();
                } else if (position == SIGNUP) {
                    isLoginForm = false;
                    signupVisible();
                }
            }
        });

        segmentLoginGroup.setPosition(isLoginForm ? 1 : 0);
    }

    private void signupVisible() {
        tabLogin.setTypeface(Constants.SFTEXT_REGULAR);
        tabSignup.setTypeface(Constants.SFTEXT_BOLD);
        llLogin.setVisibility(View.GONE);
        llSignup.setVisibility(View.VISIBLE);
    }

    private void loginVisible() {
        tabLogin.setTypeface(Constants.SFTEXT_BOLD);
        tabSignup.setTypeface(Constants.SFTEXT_REGULAR);
        llLogin.setVisibility(View.VISIBLE);
        llSignup.setVisibility(View.GONE);
    }

    public String getEmail() {
        return isLoginForm ? etEmail.getText().toString() : etSEmail.getText().toString();
    }

    public String getPassword() {
        return isLoginForm ? etPassword.getText().toString() : etSPassword.getText().toString();
    }

    private String getName() {
        return etUsername.getText().toString();
    }

    public void login() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<UserModel> call = getService().login(getEmail(), getPassword(), getToken(), Constants.DEVICE_TYPE);
        call.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                UserModel userModel = response.body();
                if (userModel != null) {
                    if (checkStatus(userModel)) {
                        if (userModel.data != null) {
                            afterLoginOrSignup(userModel.data);
                        }
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                failureError("login failed");
            }
        });
    }

    public void register() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<UserModel> call = getService().register(getName(), getEmail(), getPassword(), getToken(), Constants.DEVICE_TYPE);
        call.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                UserModel userModel = response.body();
                if (userModel != null) {
                    if (checkStatus(userModel)) {
                        if (userModel.data != null) {
                            afterLoginOrSignup(userModel.data);
                        }
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                failureError("register failed");
            }
        });
    }

    public void afterLoginOrSignup(UserModel.Data data) {
        Preferences.writeBoolean(LoginSignUpActivity.this, Constants.IS_LOGIN, true);
        Preferences.saveUserData(LoginSignUpActivity.this, data);

        //TODO :: Subscription TRUE for now.
        Preferences.writeBoolean(LoginSignUpActivity.this, Constants.INVESTING_SUBSCRIPTION, true);
        Preferences.writeBoolean(LoginSignUpActivity.this, Constants.SIGNAL_SUBSCRIPTION, true);

        if (isFirstTime()) {
            Preferences.writeBoolean(LoginSignUpActivity.this, Constants.IS_FIRST, false);
            gotoMainActivity(Constants.TAB_HOME);
        } else {
            finish();
            finishToRight();
        }
    }

    @OnClick({R.id.img_back, R.id.btn_login, R.id.btn_signup, R.id.tv_forgot_password})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tv_forgot_password:
                showForgotPasswordDialog();
                break;
            case R.id.btn_login:
                if (validLoginData()) {
                    login();
                }
                break;
            case R.id.btn_signup:
                if (validSignUpData()) {
                    register();
                }
                break;
        }
    }

    public boolean validLoginData() {
        if (!isValidEmail(getEmail())) {
            validationError("Enter Valid Email");
            return false;
        }

        if (isEmpty(getPassword())) {
            validationError("Enter Password");
            return false;
        }

        return true;
    }

    public boolean validSignUpData() {
        if (isEmpty(getName())) {
            validationError("Enter Username");
            return false;
        }

        if (!isValidEmail(getEmail())) {
            validationError("Enter Valid Email");
            return false;
        }

        if (isEmpty(getPassword())) {
            validationError("Enter Password");
            return false;
        }

        return true;
    }

    public void showForgotPasswordDialog() {
        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_forgot_password);
        dialog.setCancelable(true);
        Button btnCancel = dialog.findViewById(R.id.btn_cancel);
        Button btnReset = dialog.findViewById(R.id.btn_reset);
        final EditText etEmail = dialog.findViewById(R.id.et_email);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidEmail(etEmail.getText().toString())) {
                    Utils.hideSoftKeyboard(LoginSignUpActivity.this);
                    forgotPassword(etEmail.getText().toString(), false);
                    dialog.dismiss();
                } else {
                    Toast.makeText(LoginSignUpActivity.this, "Enter valid email", Toast.LENGTH_SHORT).show();
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void forgotPassword(final String email, final boolean isResend) {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().forgotPassword(email);
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (response.body() != null) {
                    if (checkStatus(response.body())) {
                        Toast.makeText(LoginSignUpActivity.this, response.body().msg, Toast.LENGTH_LONG).show();
                        if (!isResend)
                            showSecurityCodeDialog(email);
                    } else {
                        Toast.makeText(LoginSignUpActivity.this, response.body().msg, Toast.LENGTH_SHORT).show();
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("forgot password failed");
            }
        });
    }

    public void showSecurityCodeDialog(final String email) {
        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_security_code);
        dialog.setCancelable(true);
        Button btnCancel = dialog.findViewById(R.id.btn_cancel);
        Button btnReset = dialog.findViewById(R.id.btn_reset);
        final EditText etSecurityCode = dialog.findViewById(R.id.et_security_code);
        final EditText etNewPassword = dialog.findViewById(R.id.et_new_password);
        TextView tvResendCode = dialog.findViewById(R.id.tv_resend_code);
        tvResendCode.setPaintFlags(tvResendCode.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        tvResendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgotPassword(email, true);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEmpty(etSecurityCode.getText().toString())) {
                    Toast.makeText(LoginSignUpActivity.this, "Please enter code", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (isEmpty(etNewPassword.getText().toString())) {
                    Toast.makeText(LoginSignUpActivity.this, "Please enter password", Toast.LENGTH_SHORT).show();
                    return;
                }

                Utils.hideSoftKeyboard(LoginSignUpActivity.this);
                resetPassword(email, etSecurityCode.getText().toString(), etNewPassword.getText().toString(), dialog);
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void resetPassword(String email, String otp, String password, final Dialog dialog) {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().resetPassword(email, otp, password);
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (response.body() != null) {
                    if (checkStatus(response.body())) {
                        Toast.makeText(LoginSignUpActivity.this, response.body().msg, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    } else {
                        failureError(response.body().msg);
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("update password failed");
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
