package com.redline.coin.ui;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.redline.coin.R;
import com.redline.coin.adapter.DialogListAdapter;
import com.redline.coin.adapter.TopInvestingAdapter;
import com.redline.coin.model.Coin;
import com.redline.coin.model.CoinInfoModel;
import com.redline.coin.util.Constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InvestingActivity extends BaseActivity {

    @BindView(R.id.tv_duration)
    TextView tvDuration;
    @BindView(R.id.rv_coin)
    RecyclerView rvCoin;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    private TopInvestingAdapter mAdapter;
    private List<Coin.Data> coinData;
    private String[] durationArray;
    private int selectedPosition = 0; // long = 1, short = 0
    private boolean isRefresh = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_investing);
        ButterKnife.bind(this);

        rvCoin.setLayoutManager(new LinearLayoutManager(this));

        getCoinValue();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getCoinValue();
            }
        });
    }

    public void getCoinValue() {
        if (!isNetworkConnected()) {
            if (swipeRefreshLayout != null)
                swipeRefreshLayout.setRefreshing(false);
            return;
        }

        if (swipeRefreshLayout != null && !swipeRefreshLayout.isRefreshing())
            showProgress();

        Call<Coin> call = getService().getCoinValue(getTerm(), getAccessToken());
        call.enqueue(new Callback<Coin>() {
            @Override
            public void onResponse(Call<Coin> call, Response<Coin> response) {
                Coin coin = response.body();
                if (coin != null) {
                    if (checkStatus(coin)) {
                        coinData = new ArrayList<>();
                        coinData = coin.data;
                        if (coinData != null && coinData.size() > 0) {
                            for (int i = 0; i < coinData.size(); i++) {
                                coinData.get(i).srNo = String.valueOf(i+1);
                            }
                        }
                        if (!isInvestingSubscribed()) {
                            if (coinData.size() >= 3) {
                                coinData.remove(0);
                                coinData.remove(0);
                                coinData.remove(0);
                            }
                            coinData.add(0, null);
                        }
                        setAdapter();
                    } else {
                        isRefresh = true;
                    }
                }

                hideProgress();
                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<Coin> call, Throwable t) {
                failureError("get coin failed");
                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    public void setAdapter() {
        if (coinData != null && coinData.size() > 0) {
            if (mAdapter == null) {
                mAdapter = new TopInvestingAdapter(this);
            }

            mAdapter.doRefresh(coinData);

            if (rvCoin != null && rvCoin.getAdapter() == null) {
                rvCoin.setAdapter(mAdapter);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isRefresh) {
            isRefresh = false;
            getCoinValue();
        }
    }

    @OnClick({R.id.img_info, R.id.tv_duration})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_info:
                showInfoDialog(0, getDialogInfoData());
                break;
            case R.id.tv_duration:
                showDurationDialog();
                break;
        }
    }

    private int getTerm() {
        return selectedPosition == 0 ? Constants.LONG_TERM : Constants.SHORT_TERM;
    }

    public void showDurationDialog() {
        durationArray = new String[]{ getString(R.string.long_term), getString(R.string.short_term) };

        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_list_select);
        dialog.setCancelable(true);

        final TextView tvTitle = dialog.findViewById(R.id.tv_title);
        RecyclerView rvList = dialog.findViewById(R.id.rv_list);
        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);

        tvTitle.setText(getString(R.string.term));
        rvList.setLayoutManager(new LinearLayoutManager(this));

        DialogListAdapter adapter = new DialogListAdapter(this, Arrays.asList(durationArray), selectedPosition,
                new DialogListAdapter.OnItemSelect() {
                    @Override
                    public void itemSelect(int position) {
                        selectedPosition = position;
                        String[] split = durationArray[position].split(getString(R.string.investments));
                        tvDuration.setText(split[0].trim() + " " + split[1].trim());
                        dialog.dismiss();
                        getCoinValue();
                    }
                });
        rvList.setAdapter(adapter);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public ArrayList<CoinInfoModel> getDialogInfoData() {
        int[] colorList = {R.color.black};
        String[] words = {Constants.REDLINE_COIN};
        String[] fonts = {Constants.SFTEXT_MEDIUM};

        ArrayList<CoinInfoModel> coinInfoModels = new ArrayList<>();
        coinInfoModels.add(new CoinInfoModel(getString(R.string.top_investing_info_text), fonts, colorList, words));
        return coinInfoModels;
    }
}
