package com.redline.coin.ui;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TabHost;
import android.widget.Toast;

import com.redline.coin.R;
import com.redline.coin.util.Constants;
import com.redline.coin.util.Preferences;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends TabActivity implements TabHost.OnTabChangeListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    @BindView(android.R.id.tabhost)
    TabHost mTabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setTab("home", R.drawable.home_tab, HomeActivity.class);
        setTab("investing", R.drawable.investing_tab, InvestingActivity.class);
        setTab("news", R.drawable.signal_tab, SignalActivity.class);
        setTab("alert", R.drawable.alert_tab, AlertActivity.class);
        setTab("more", R.drawable.more_tab, MoreActivity.class);

        mTabHost.setOnTabChangedListener(this);

        if (getIntent() != null) {
            int screen = getIntent().getIntExtra(Constants.SCREEN_NAME, 0);
            mTabHost.setCurrentTab(screen);

            handleDataMessage(getIntent().getStringExtra("data"));
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleDataMessage(intent.getStringExtra("data"));
    }

    private void handleDataMessage(String jsonData) {
        if (TextUtils.isEmpty(jsonData)) {
            return;
        }
        Log.e(TAG, "push json: " + jsonData);

        try {
            JSONObject json = new JSONObject(jsonData);
            if (json.has("additionalData")) {
                String additionalData = json.getString("additionalData");
                if (!TextUtils.isEmpty(additionalData)) {
                    JSONObject data = new JSONObject(additionalData);
                    String m_type = data.getString("m_type");
                    if (!TextUtils.isEmpty(m_type)) {
                        if (m_type.equalsIgnoreCase("signal")) {
                            String signalId = data.getString("signal_id");
                            if (!TextUtils.isEmpty(signalId)) {
                                Preferences.writeString(MainActivity.this, Constants.SIGNAL_ID, signalId);
                                mTabHost.setCurrentTab(Constants.TAB_SIGNAL);
                            }
                        } else if (m_type.equalsIgnoreCase("new signal")) {
                            Preferences.writeString(MainActivity.this, Constants.SIGNAL_ID, "");
                            mTabHost.setCurrentTab(Constants.TAB_SIGNAL);
                        } else {
                            Preferences.writeBoolean(MainActivity.this, Constants.NOTIFICATION_BUYORNOT, true);
                            mTabHost.setCurrentTab(Constants.TAB_HOME);
                        }
                    }
                }
            }
        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    public void setTab(String tag, int drawable, Class<?> activityClass) {
        TabHost.TabSpec tabSpec = mTabHost.newTabSpec(tag)
                .setIndicator("", ContextCompat.getDrawable(this, drawable))
                .setContent(new Intent(this, activityClass));
        mTabHost.addTab(tabSpec);
    }

    @Override
    public void onTabChanged(String tabId) {

    }

    public void gotoMainActivity(int screen) {
        Intent i = new Intent(this, MainActivity.class);
        i.putExtra(Constants.SCREEN_NAME, screen);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }

    public void redirectActivity(Class<?> activityClass) {
        startActivity(new Intent(this, activityClass));
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
    }
}
