package com.redline.coin.ui;

import android.os.Bundle;
import android.view.View;

import com.redline.coin.R;
import com.redline.coin.util.Constants;
import com.redline.coin.util.textview.TextViewSFTextRegular;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MoreAppsActivity extends BaseActivity {

    @BindView(R.id.tv_back_text)
    TextViewSFTextRegular tvBackText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_apps);
        ButterKnife.bind(this);

        tvBackText.setText(getString(R.string.more));
    }

    @OnClick({R.id.ll_back, R.id.rl_cheapest_easy, R.id.rl_24_task, R.id.rl_iform_app})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.rl_cheapest_easy:
                redirectUsingCustomTab(Constants.CHEAPEST_ESSAY_SITE);
                break;
            case R.id.rl_24_task:
                redirectUsingCustomTab(Constants.TASK24_SITE);
                break;
            case R.id.rl_iform_app:
                redirectUsingCustomTab(Constants.IFORM_SITE);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
