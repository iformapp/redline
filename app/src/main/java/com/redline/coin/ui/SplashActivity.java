package com.redline.coin.ui;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.redline.coin.R;
import com.redline.coin.api.ApiClient;
import com.redline.coin.api.ApiInterface;
import com.redline.coin.model.GeneralModel;
import com.redline.coin.model.Subscription;
import com.redline.coin.util.Constants;
import com.redline.coin.util.Preferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends BaseActivity {

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction() != null) {
                    if (intent.getAction().equals(Constants.REGISTRATION_COMPLETE)) {
                        redirectIntent();
                    }
                }
            }
        };
    }

    private void checkPermission() {
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        redirectIntent();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        redirectIntent();
                        Toast.makeText(SplashActivity.this, "Please give permission for notification sound", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {/* ... */}
                }).check();
    }

    public void redirectIntent() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isFirstTime()) {
                    redirectActivity(TutorialActivity.class);
                } else if (!isLogin()) {
                    redirectActivity(LoginSignUpActivity.class);
                } else {
                    redirectActivity(MainActivity.class);
                }
                finish();
            }
        }, 2500);
    }

    public void addSubscription() {
        if (!isNetworkConnected()) {
            return;
        }

        ApiInterface service = ApiClient.getClient().create(ApiInterface.class);
        Call<GeneralModel> call = service.addSubscription(1, Constants.DEVICE_TYPE, getToken(), getUserId());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                GeneralModel model = response.body();
                if (model != null) {
                    if (model.flag == 1) {
                        Preferences.writeBoolean(SplashActivity.this, Constants.INVESTING_SUBSCRIPTION, true);
                        Preferences.writeBoolean(SplashActivity.this, Constants.SIGNAL_SUBSCRIPTION, true);
                        checkPermission();

                        // TODO :: Remove Subscription temporary
                        //checkSubscription();
                    }
                }
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                //failureError("get coin failed");
            }
        });
    }

    public void checkSubscription() {
        if (!isNetworkConnected()) {
            return;
        }

        ApiInterface service = ApiClient.getClient().create(ApiInterface.class);
        Call<Subscription> call = service.checkSubscription(getUserId());
        call.enqueue(new Callback<Subscription>() {
            @Override
            public void onResponse(Call<Subscription> call, Response<Subscription> response) {
                Subscription model = response.body();
                if (model != null && checkStatus(model)) {
                    Preferences.writeBoolean(SplashActivity.this, Constants.INVESTING_SUBSCRIPTION,
                            model.data.topInv == 1);
                    Preferences.writeBoolean(SplashActivity.this, Constants.SIGNAL_SUBSCRIPTION,
                            model.data.signal == 1);
                }

                checkPermission();
            }

            @Override
            public void onFailure(Call<Subscription> call, Throwable t) {
                Log.e("Failure", "Subscription failed");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Constants.REGISTRATION_COMPLETE));

        if (!isEmpty(getToken())) {
            redirectIntent();
        }
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }
}
