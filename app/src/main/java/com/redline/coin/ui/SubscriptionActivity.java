package com.redline.coin.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.redline.coin.R;
import com.redline.coin.api.ApiClient;
import com.redline.coin.api.ApiInterface;
import com.redline.coin.model.Payment;
import com.redline.coin.model.Subscription;
import com.redline.coin.util.Constants;
import com.redline.coin.util.Preferences;
import com.redline.coin.util.Utils;
import com.redline.coin.util.textview.TextViewSFTextMedium;
import com.redline.coin.util.textview.TextViewSFTextRegular;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubscriptionActivity extends BaseActivity {

    private final int REQ_PAYMENT_CODE = 100;

    @BindView(R.id.tv_back_text)
    TextView tvBackText;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_month)
    TextView tvMonth;
    @BindView(R.id.tv_month_price)
    TextView tvMonthPrice;
    @BindView(R.id.tv_year)
    TextView tvYear;
    @BindView(R.id.tv_year_price)
    TextView tvYearPrice;
    @BindView(R.id.tv_subscription_note)
    TextView tvSubscriptionNote;
    @BindView(R.id.rl_month)
    RelativeLayout rlMonth;
    @BindView(R.id.rl_year)
    RelativeLayout rlYear;
    @BindView(R.id.tv_desciption)
    TextViewSFTextRegular tvDesciption;
    @BindView(R.id.tv_save_percent)
    TextViewSFTextMedium tvSavePercent;

    private boolean isInvestingSubscription;
    private String planDuration;
    private String planAmount;
    private String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription);
        ButterKnife.bind(this);

        if (getIntent() != null) {
            isInvestingSubscription = getIntent().getBooleanExtra(Constants.INVESTING_SUBSCRIPTION, false);
        }

        String text;
        String[] boldText = {"Let our algorithms and crypto traders work for you."};
        String[] fonts = {Constants.SFTEXT_SEMIBOLD};
        if (isInvestingSubscription) {
            text = getString(R.string.investments_subscription_text);
            type = Constants.TOP_INV;
            planDuration = Constants.YEARLY;
            planAmount = Constants.INVESTING_YEARLY_PRICE;
            tvBackText.setText(getString(R.string.top_investing));
            tvTitle.setText(getString(R.string.investing_subscription));
            tvSubscriptionNote.setText(getString(R.string.investing_subscription_note));
            tvMonthPrice.setText(Utils.numberFormat(Constants.INVESTING_MONTHLY_PRICE));
            tvYearPrice.setText(Utils.numberFormat(Constants.INVESTING_YEARLY_PRICE));
        } else {
            text = getString(R.string.signal_subscription_text);
            type = Constants.SIGNAL;
            planDuration = Constants.MONTHLY;
            planAmount = Constants.SIGNAL_MONTHLY_PRICE;
            tvBackText.setText(getString(R.string.signal));
            tvTitle.setText(getString(R.string.signals_subscription));
            tvSubscriptionNote.setText(getString(R.string.signal_subscription_note));
            tvMonthPrice.setText(Utils.numberFormat(Constants.SIGNAL_MONTHLY_PRICE));
            tvYearPrice.setText(Utils.numberFormat(Constants.SIGNAL_YEARLY_PRICE));
            tvSavePercent.setVisibility(View.GONE);
        }
        tvDesciption.setText(Utils.getBoldString(this, text, fonts, null, boldText));
    }

    @OnClick({R.id.ll_back, R.id.rl_month, R.id.rl_year, R.id.btn_continue, R.id.tv_privacy_policy, R.id.tv_terms_of_service})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.rl_month:
                planDuration = Constants.MONTHLY;
                if (isInvestingSubscription) {
                    planAmount = Constants.INVESTING_YEARLY_PRICE;
                } else {
                    planAmount = Constants.SIGNAL_MONTHLY_PRICE;
                }
                unSelectAll();
                selectCard(rlMonth, tvMonth, tvMonthPrice);
                break;
            case R.id.rl_year:
                planDuration = Constants.YEARLY;
                if (isInvestingSubscription) {
                    planAmount = Constants.INVESTING_YEARLY_PRICE;
                } else {
                    planAmount = Constants.SIGNAL_YEARLY_PRICE;
                }
                unSelectAll();
                selectCard(rlYear, tvYear, tvYearPrice);
                break;
            case R.id.btn_continue:
                makePayment();
                break;
            case R.id.tv_privacy_policy:
                redirectUsingCustomTab(Constants.POLICY);
                break;
            case R.id.tv_terms_of_service:
                redirectUsingCustomTab(Constants.TERMS_SERVICE);
                break;
        }
    }

    public void makePayment() {
        if (!isNetworkConnected()) {
            return;
        }

        showProgress();

        ApiInterface service = ApiClient.getClient().create(ApiInterface.class);
        Call<Payment> call = service.addPayment(getUserId(), type, planDuration, planAmount);
        call.enqueue(new Callback<Payment>() {
            @Override
            public void onResponse(Call<Payment> call, Response<Payment> response) {
                Payment model = response.body();
                if (model != null && checkStatus(model)) {
                    if (!TextUtils.isEmpty(model.url)) {
                        Intent i = new Intent(SubscriptionActivity.this, PayPalPaymentActivity.class);
                        i.putExtra(Constants.PAYPAL_URL, model.url);
                        startActivityForResult(i, REQ_PAYMENT_CODE);
                    } else {
                        failureError("Url not valid");
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<Payment> call, Throwable t) {
                failureError("payment failed");
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQ_PAYMENT_CODE) {
            checkSubscription();
        }
    }

    public void checkSubscription() {
        if (!isNetworkConnected()) {
            return;
        }

        showProgress();

        ApiInterface service = ApiClient.getClient().create(ApiInterface.class);
        Call<Subscription> call = service.checkSubscription(getUserId());
        call.enqueue(new Callback<Subscription>() {
            @Override
            public void onResponse(Call<Subscription> call, Response<Subscription> response) {
                Subscription model = response.body();
                if (model != null && model.flag == 1) {
                    Preferences.writeBoolean(SubscriptionActivity.this, Constants.INVESTING_SUBSCRIPTION,
                            model.data.topInv == 1);
                    Preferences.writeBoolean(SubscriptionActivity.this, Constants.SIGNAL_SUBSCRIPTION,
                            model.data.signal == 1);
                }
                hideProgress();
                gotoMainActivity(isInvestingSubscription ? Constants.TAB_INVESTING : Constants.TAB_SIGNAL);
            }

            @Override
            public void onFailure(Call<Subscription> call, Throwable t) {
                failureError("Subscription failed");
            }
        });
    }

    private void unSelectAll() {
        rlMonth.setBackgroundColor(Color.TRANSPARENT);
        rlYear.setBackgroundColor(Color.TRANSPARENT);
        Typeface tf = Typeface.createFromAsset(getAssets(), Constants.SFTEXT_LIGHT);
        tvMonth.setTypeface(tf);
        tvMonthPrice.setTypeface(tf);
        tvYear.setTypeface(tf);
        tvYearPrice.setTypeface(tf);
    }

    private void selectCard(RelativeLayout relativeLayout, TextView tvDuration, TextView tvPrice) {
        relativeLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.card_bg_gray));
        Typeface tf = Typeface.createFromAsset(getAssets(), Constants.SFTEXT_BOLD);
        tvDuration.setTypeface(tf);
        tvPrice.setTypeface(tf);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
