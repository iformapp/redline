package com.redline.coin.ui;

import android.os.Bundle;
import android.view.View;

import com.redline.coin.R;
import com.redline.coin.util.Constants;
import com.redline.coin.util.Preferences;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class AgreeActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agree);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.tv_disclaimer, R.id.btn_agree})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_disclaimer:
                showDisclaimerDialog();
                break;
            case R.id.btn_agree:
                Preferences.writeBoolean(this, Constants.IS_FIRST, false);
                gotoMainActivity(Constants.TAB_HOME);
                break;
        }
    }
}
