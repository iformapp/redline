package com.redline.coin.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.redline.coin.R;
import com.redline.coin.model.GeneralModel;
import com.redline.coin.util.edittext.EditTextSFDisplayRegular;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdatePasswordActivity extends BaseActivity {

    @BindView(R.id.et_old_password)
    EditTextSFDisplayRegular etOldPassword;
    @BindView(R.id.et_new_password)
    EditTextSFDisplayRegular etNewPassword;
    @BindView(R.id.et_confirm_password)
    EditTextSFDisplayRegular etConfirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_password);
        ButterKnife.bind(this);
    }

    public String getOldPassword() {
        return etOldPassword.getText().toString().trim();
    }

    public String getNewPassword() {
        return etNewPassword.getText().toString().trim();
    }

    public String getConfirmPassword() {
        return etConfirmPassword.getText().toString().trim();
    }

    public void savePassword() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().changePassword(getAccessToken(), getOldPassword(), getNewPassword());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (response.body() != null) {
                    if (checkStatus(response.body())) {
                        Toast.makeText(UpdatePasswordActivity.this, response.body().msg, Toast.LENGTH_SHORT).show();
                        finish();
                        finishToRight();
                    } else {
                        Toast.makeText(UpdatePasswordActivity.this, response.body().msg, Toast.LENGTH_SHORT).show();
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("update password failed");
            }
        });
    }

    @OnClick({R.id.ll_back, R.id.tv_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.tv_save:
                if (isValid()) {
                    savePassword();
                }
                break;
        }
    }

    private boolean isValid() {
        if (isEmpty(getOldPassword())) {
            validationError(getString(R.string.enter_your_old_password));
            return false;
        }

        if (isEmpty(getNewPassword())) {
            validationError(getString(R.string.enter_your_new_password));
            return false;
        }

        if (isEmpty(getConfirmPassword())) {
            validationError(getString(R.string.enter_confirm_password));
            return false;
        }

        if (!getNewPassword().equals(getConfirmPassword())) {
            validationError(getString(R.string.doesnt_match_password));
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
