package com.redline.coin.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.redline.coin.R;
import com.redline.coin.adapter.InfoPagerAdapter;
import com.redline.coin.api.ApiClient;
import com.redline.coin.api.ApiInterface;
import com.redline.coin.model.CoinInfoModel;
import com.redline.coin.model.GeneralModel;
import com.redline.coin.model.UserModel;
import com.redline.coin.util.Constants;
import com.redline.coin.util.Preferences;
import com.victor.loading.rotate.RotateLoading;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

public class BaseActivity extends AppCompatActivity {

    private Dialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    public ApiInterface getService() {
        return ApiClient.getClient().create(ApiInterface.class);
    }

    public void failureError(String message) {
        Toast toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        TextView v = toast.getView().findViewById(android.R.id.message);
        if( v != null) v.setGravity(Gravity.CENTER);
        toast.show();
        hideProgress();
    }

    public void validationError(String message) {
        Toast toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        TextView v = toast.getView().findViewById(android.R.id.message);
        if( v != null) v.setGravity(Gravity.CENTER);
        toast.show();
    }

    public String getToken() {
        return Preferences.readString(this, Constants.FCM_TOKEN, "");
    }

    public String getUserId() {
        UserModel.Data userData = Preferences.getUserData(this);
        return userData != null ? userData.userId : "";
    }

    public String getAccessToken() {
        UserModel.Data userData = Preferences.getUserData(this);
        return userData != null ? userData.accessToken : "";
    }

    public boolean isLogin() {
        return Preferences.readBoolean(this, Constants.IS_LOGIN, false);
    }

    public boolean isFirstTime() {
        return Preferences.readBoolean(this, Constants.IS_FIRST, true);
    }

    public String getGAID() {
        return Preferences.readString(this, Constants.GAID, "");
    }

    public boolean isSignalSubscribed() {
        return Preferences.readBoolean(this, Constants.SIGNAL_SUBSCRIPTION, false);
    }

    public boolean isInvestingSubscribed() {
        return Preferences.readBoolean(this, Constants.INVESTING_SUBSCRIPTION, false);
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null && cm.getActiveNetworkInfo() != null) {
            return true;
        }
        Toast.makeText(this, "connect to internet", Toast.LENGTH_SHORT).show();
        return false;
    }

    public boolean checkStatus(GeneralModel model) {
        if (model == null) {
            return false;
        }

        if (model.flag == 1) {
            return true;
        } else if (model.flag == 3) {
            Preferences.saveUserData(this, null);
            goToLoginSignup(true);
            return false;
        }
        failureError(model.msg);
        return false;
    }

    public void gotoMainActivity(int screen) {
        if (getParent() != null) {
            ((MainActivity) getParent()).gotoMainActivity(screen);
        } else {
            Intent i = new Intent(this, MainActivity.class);
            i.putExtra(Constants.SCREEN_NAME, screen);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
            finishToRight();
        }
    }

    public void goToLoginSignup(boolean isLogin) {
        Intent i = new Intent(this, LoginSignUpActivity.class);
        i.putExtra(Constants.FROM_LOGIN, isLogin);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        openToTop();
    }

    public void redirectTab(int tabIndex) {
        if (getParent() != null) {
            ((MainActivity) getParent()).getTabHost().setCurrentTab(tabIndex);
        }
    }

    public void redirectActivity(Class<?> activityClass) {
        if (getParent() != null) {
            ((MainActivity) getParent()).redirectActivity(activityClass);
        } else {
            startActivity(new Intent(this, activityClass));
            openToLeft();
        }
    }

    public void openToTop() {
        overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
    }

    public void openToLeft() {
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
    }

    public void finishToBottom() {
        overridePendingTransition(R.anim.stay, R.anim.slide_out_down);
    }

    public void finishToRight() {
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }

    public void showProgress() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_loading);
        dialog.setCancelable(false);
        RotateLoading rotateLoading = dialog.findViewById(R.id.rotateloading);
        rotateLoading.start();
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    public void hideProgress() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public boolean isEmpty(String s) {
        return TextUtils.isEmpty(s);
    }

    public void showDisclaimerDialog() {
        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_disclaimer);
        dialog.setCancelable(true);

        ImageView imgDown = dialog.findViewById(R.id.img_back);
        Button btnBack = dialog.findViewById(R.id.btn_back);

        imgDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void showInfoDialog(int position, final ArrayList<CoinInfoModel> dialogInfoData) {
        final Dialog infoDialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        infoDialog.setTitle(null);
        infoDialog.setContentView(R.layout.dialog_info_pager);
        infoDialog.setCancelable(true);

        final ViewPager viewPager = infoDialog.findViewById(R.id.viewPager);
        ImageView imgBack = infoDialog.findViewById(R.id.img_back);
        final ImageView imgPrevious = infoDialog.findViewById(R.id.img_previous);
        final ImageView imgNext = infoDialog.findViewById(R.id.img_next);
        CircleIndicator indicator = infoDialog.findViewById(R.id.indicator);

        InfoPagerAdapter adapter = new InfoPagerAdapter(this, dialogInfoData);
        if (viewPager != null) {
            viewPager.setAdapter(adapter);

            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    if (position == 0) {
                        imgPrevious.setVisibility(View.GONE);
                        imgNext.setVisibility(View.VISIBLE);
                    } else if (position == dialogInfoData.size() - 1) {
                        imgPrevious.setVisibility(View.VISIBLE);
                        imgNext.setVisibility(View.GONE);
                    } else {
                        imgPrevious.setVisibility(View.VISIBLE);
                        imgNext.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            viewPager.setCurrentItem(position);
            viewPager.setOffscreenPageLimit(dialogInfoData.size() - 1);
        }
        indicator.setViewPager(viewPager);

        if (dialogInfoData.size() == 1) {
            imgPrevious.setVisibility(View.GONE);
            imgNext.setVisibility(View.GONE);
            indicator.setVisibility(View.GONE);
        } else {
            if (position == 0) {
                imgPrevious.setVisibility(View.GONE);
                imgNext.setVisibility(View.VISIBLE);
            } else if (position == dialogInfoData.size() - 1) {
                imgPrevious.setVisibility(View.VISIBLE);
                imgNext.setVisibility(View.GONE);
            } else {
                imgPrevious.setVisibility(View.VISIBLE);
                imgNext.setVisibility(View.VISIBLE);
            }
        }

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                infoDialog.dismiss();
            }
        });

        imgPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewPager != null) {
                    viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
                }
            }
        });

        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewPager != null) {
                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(infoDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        infoDialog.show();
        infoDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        infoDialog.getWindow().setAttributes(lp);
    }

    public ArrayList<CoinInfoModel> getDialogInfoData() {
        int[] colorList = {R.color.black, R.color.light_green, R.color.blue_dark, R.color.light_red};
        String[] words = {Constants.REDLINE_COIN, Constants.S_BUY, Constants.S_HOLD, Constants.S_DONT_BUY};
        String[] fonts = {Constants.SFTEXT_MEDIUM};

        ArrayList<CoinInfoModel> coinInfoModels = new ArrayList<>();
        coinInfoModels.add(new CoinInfoModel(getString(R.string.info_item_text1), fonts, colorList, words));
        coinInfoModels.add(new CoinInfoModel("", getString(R.string.strong_buy), getString(R.string.strong_buy_text),
                R.drawable.strong_buy));
        coinInfoModels.add(new CoinInfoModel("", getString(R.string.buy), getString(R.string.buy_text),
                R.drawable.buy));
        coinInfoModels.add(new CoinInfoModel("", getString(R.string.hold), getString(R.string.hold_text),
                R.drawable.hold));
        coinInfoModels.add(new CoinInfoModel("", getString(R.string.don_t_buy), getString(R.string.dont_buy_text),
                R.drawable.dont_buy));
        coinInfoModels.add(new CoinInfoModel("", getString(R.string.strong_don_t_buy), getString(R.string.strong_dont_buy_text),
                R.drawable.strong_dont_buy));
        return coinInfoModels;
    }

    public void redirectUsingCustomTab(String url) {
        try {
            Uri uri = Uri.parse(url);
            CustomTabsIntent.Builder intentBuilder = new CustomTabsIntent.Builder();
            intentBuilder.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary));
            intentBuilder.setSecondaryToolbarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            CustomTabsIntent customTabsIntent = intentBuilder.build();
            customTabsIntent.launchUrl(this, uri);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "something went wrong", Toast.LENGTH_SHORT).show();
        }
    }

    public void shareApp() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=" + getPackageName());
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Share with..."));
    }
}
