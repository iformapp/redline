package com.redline.coin.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.redline.coin.R;
import com.redline.coin.model.Signals;
import com.redline.coin.ui.AlertDetailsActivity;
import com.redline.coin.util.Constants;
import com.redline.coin.util.Utils;
import com.redline.coin.util.textview.TextViewSFTextRegular;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OpenSignalAdapter extends RecyclerView.Adapter<OpenSignalAdapter.SimpleViewHolder> {

    private Context mContext;
    private List<Signals.Data> signalsData;

    public OpenSignalAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.open_signal_item, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SimpleViewHolder holder, final int position) {
        Signals.Data data = signalsData.get(position);
        Utils.loadImage(mContext, Constants.IMAGE_URL + data.imgUrl, holder.imgLogo);
        holder.tvCoinName.setText(data.coinName);
    }

    @Override
    public int getItemCount() {
        return signalsData != null ? signalsData.size() : 0;
    }

    public void doRefresh(List<Signals.Data> coinData) {
        this.signalsData = coinData;
        notifyDataSetChanged();
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_logo)
        ImageView imgLogo;
        @BindView(R.id.tv_coin_name)
        TextViewSFTextRegular tvCoinName;

        SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(mContext, AlertDetailsActivity.class);
                    i.putExtra(Constants.SIGNAL_ID, signalsData.get(getAdapterPosition()).signalId);
                    mContext.startActivity(i);
                }
            });
        }
    }
}
