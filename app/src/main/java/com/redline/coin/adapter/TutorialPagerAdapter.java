package com.redline.coin.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.redline.coin.R;
import com.redline.coin.ui.BaseActivity;
import com.redline.coin.ui.LoginSignUpActivity;
import com.redline.coin.ui.TutorialActivity;
import com.redline.coin.util.Constants;
import com.redline.coin.util.Preferences;
import com.redline.coin.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TutorialPagerAdapter extends PagerAdapter {

    @BindView(R.id.ll_bitcoin)
    LinearLayout llBitcoin;
    @BindView(R.id.img_mobile)
    ImageView imgMobile;
    @BindView(R.id.ll_mobile)
    LinearLayout llMobile;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_desciption)
    TextView tvDesciption;
    @BindView(R.id.ll_first_tab)
    LinearLayout llFirstTab;
    @BindView(R.id.tv_disclaimer)
    TextView tvDisclaimer;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.rl_bottom)
    RelativeLayout rlBottom;
    @BindView(R.id.rl_last_tab)
    RelativeLayout rlLastTab;

    private Context mContext;

    public TutorialPagerAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup v = (ViewGroup) inflater.inflate(R.layout.fragment_tutorial, collection, false);
        ButterKnife.bind(this, v);

        if (position == 0) {
            rlLastTab.setVisibility(View.GONE);
            llFirstTab.setVisibility(View.VISIBLE);
            llMobile.setVisibility(View.GONE);
            llBitcoin.setVisibility(View.VISIBLE);
            tvTitle.setText(mContext.getString(R.string.what_s_redline_coin));
            tvDesciption.setText(mContext.getString(R.string.description_text1));
        } else if (position == 1) {
            rlLastTab.setVisibility(View.GONE);
            llFirstTab.setVisibility(View.VISIBLE);
            llMobile.setVisibility(View.VISIBLE);
            llBitcoin.setVisibility(View.GONE);
            Utils.loadImage(mContext, R.drawable.screen1, imgMobile);
            tvTitle.setText(mContext.getString(R.string.what_s_buy_or_not));
            String s = mContext.getString(R.string.description_text2);
            int[] colorList = {R.color.light_green, R.color.blue_dark, R.color.light_red};
            String[] words = {Constants.S_BUY, Constants.S_HOLD, Constants.S_DONT_BUY};
            String[] fonts = {Constants.SFTEXT_SEMIBOLD, Constants.SFTEXT_SEMIBOLD, Constants.SFTEXT_SEMIBOLD};
            tvDesciption.setText(Utils.getBoldString(mContext, s, fonts, colorList, words));
        } else if (position == 2) {
            rlLastTab.setVisibility(View.GONE);
            llFirstTab.setVisibility(View.VISIBLE);
            llMobile.setVisibility(View.VISIBLE);
            llBitcoin.setVisibility(View.GONE);
            Utils.loadImage(mContext, R.drawable.screen2, imgMobile);
            tvTitle.setText(mContext.getString(R.string.what_s_top_investing));
            String s = mContext.getString(R.string.description_text3);
            String[] words = { "Top Investing" };
            String[] fonts = {Constants.SFTEXT_SEMIBOLD};
            tvDesciption.setText(Utils.getBoldString(mContext, s, fonts, null, words));
        } else if (position == 3) {
            rlLastTab.setVisibility(View.GONE);
            llFirstTab.setVisibility(View.VISIBLE);
            llMobile.setVisibility(View.VISIBLE);
            llBitcoin.setVisibility(View.GONE);
            Utils.loadImage(mContext, R.drawable.screen3, imgMobile);
            tvTitle.setText(mContext.getString(R.string.what_s_signal));
            String s = mContext.getString(R.string.description_text4);
            String[] words = { "Signal" };
            int[] color = { R.color.black };
            String[] fonts = { Constants.SFTEXT_SEMIBOLD };
            tvDesciption.setText(Utils.getBoldString(mContext, s, fonts, color, words));
        } else {
            rlLastTab.setVisibility(View.VISIBLE);
            llFirstTab.setVisibility(View.GONE);
            tvDisclaimer.setPaintFlags(tvDisclaimer.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        }
        collection.addView(v);
        return v;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

    @OnClick({R.id.img_back, R.id.btn_agree, R.id.tv_disclaimer})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                ((TutorialActivity) mContext).changePager();
                break;
            case R.id.btn_agree:
                Intent i = new Intent(mContext, LoginSignUpActivity.class);
                //i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(i);
                ((BaseActivity) mContext).openToLeft();
                break;
            case R.id.tv_disclaimer:
                ((BaseActivity) mContext).showDisclaimerDialog();
                break;
        }
    }
}
