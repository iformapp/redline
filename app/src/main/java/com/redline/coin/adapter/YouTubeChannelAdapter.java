package com.redline.coin.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;

import com.redline.coin.R;
import com.redline.coin.model.YouTubeChannel.Data;
import com.redline.coin.ui.TopYouTubeActivity;
import com.redline.coin.util.Utils;
import com.redline.coin.util.textview.TextViewSFTextBold;
import com.redline.coin.util.textview.TextViewSFTextMedium;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class YouTubeChannelAdapter extends RecyclerView.Adapter<YouTubeChannelAdapter.SimpleViewHolder>
        implements Filterable {

    private Context mContext;
    private List<Data> signalsData;
    private List<Data> mDatasetFiltered;

    public YouTubeChannelAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_top_youtube, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SimpleViewHolder holder, final int position) {
        Data data = mDatasetFiltered.get(position);
        holder.tvNo.setText((position + 1) + "");
        holder.tvChannelName.setText(data.title);
        holder.tvRate.setText(data.likes + "");
        holder.tvSubscribers.setText(Utils.convertToSuffix(Long.parseLong(data.subscribercount)));
        holder.tvUploads.setText(Utils.convertToSuffix(Long.parseLong(data.videocount)));

        holder.llOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((TopYouTubeActivity) mContext).onItemClick(mDatasetFiltered, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDatasetFiltered != null ? mDatasetFiltered.size() : 0;
    }

    public void doRefresh(List<Data> coinData) {
        this.signalsData = coinData;
        this.mDatasetFiltered = coinData;
        notifyDataSetChanged();
    }

    public void refreshFilterList(List<Data> coinData) {
        this.mDatasetFiltered = coinData;
        notifyDataSetChanged();
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_no)
        TextViewSFTextBold tvNo;
        @BindView(R.id.tv_channel_name)
        TextViewSFTextMedium tvChannelName;
        @BindView(R.id.ll_open)
        LinearLayout llOpen;
        @BindView(R.id.tv_subscribers)
        TextViewSFTextBold tvSubscribers;
        @BindView(R.id.tv_uploads)
        TextViewSFTextBold tvUploads;
        @BindView(R.id.tv_rate)
        TextViewSFTextBold tvRate;

        SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mDatasetFiltered = signalsData;
                } else {
                    List<Data> filteredList = new ArrayList<>();
                    for (Data row : signalsData) {
                        if (row.title.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    mDatasetFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mDatasetFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mDatasetFiltered = (List<Data>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
