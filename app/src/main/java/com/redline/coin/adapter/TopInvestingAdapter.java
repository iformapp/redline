package com.redline.coin.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.redline.coin.R;
import com.redline.coin.model.Coin;
import com.redline.coin.ui.SubscriptionActivity;
import com.redline.coin.util.Constants;
import com.redline.coin.util.Utils;
import com.redline.coin.util.button.ButtonSFTextBold;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TopInvestingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Coin.Data> coinData;
    private int ITEM_SUBSCRIPTION = 1;
    private int ITEM_VIEW = 2;

    public TopInvestingAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == ITEM_VIEW) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_investing, parent, false);
            return new SimpleViewHolder(view);
        } else if (viewType == ITEM_SUBSCRIPTION) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.investing_subscription_view, parent, false);
            return new SubscriptionViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder mHolder, int position) {
        if (mHolder instanceof SimpleViewHolder) {
            SimpleViewHolder holder = (SimpleViewHolder) mHolder;
            Coin.Data data = coinData.get(position);

            holder.tvNo.setText(data.srNo);
            Utils.loadImage(mContext, Constants.IMAGE_URL + data.imgUrl, holder.imgLogo);
            holder.tvCoinName.setText(data.name + " - ");
            holder.tvShortName.setText(data.webId);
            holder.tvPrice.setText(Utils.numberFormat(data.values));
            if (Double.valueOf(data.updateValue) >= 0) {
                holder.tvPercent.setTextColor(ContextCompat.getColor(mContext, R.color.light_green));
                holder.tvPercent.setText("+" + Utils.numberFormatPercentage(data.updateValue) + "%");
            } else {
                holder.tvPercent.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                holder.tvPercent.setText(Utils.numberFormatPercentage(data.updateValue) + "%");
            }
            holder.tvUpdateTime.setText("Update: " + data.lastUpdate.toLowerCase() + " ago");
        } else {
            SubscriptionViewHolder holder = (SubscriptionViewHolder) mHolder;
            holder.btnSubscribing.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(mContext, SubscriptionActivity.class);
                    i.putExtra(Constants.INVESTING_SUBSCRIPTION, true);
                    mContext.startActivity(i);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return coinData != null ? coinData.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return coinData.get(position) != null ? ITEM_VIEW : ITEM_SUBSCRIPTION;
    }

    public void doRefresh(List<Coin.Data> coinData) {
        this.coinData = coinData;
        notifyDataSetChanged();
    }

    static class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_no)
        TextView tvNo;
        @BindView(R.id.img_logo)
        ImageView imgLogo;
        @BindView(R.id.tv_coin_name)
        TextView tvCoinName;
        @BindView(R.id.tv_short_name)
        TextView tvShortName;
        @BindView(R.id.tv_update_time)
        TextView tvUpdateTime;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.tv_percent)
        TextView tvPercent;
        @BindView(R.id.ll_main)
        LinearLayout llMain;

        SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    static class SubscriptionViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.btn_subscribing)
        ButtonSFTextBold btnSubscribing;

        SubscriptionViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
