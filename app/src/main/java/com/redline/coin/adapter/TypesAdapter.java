package com.redline.coin.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;

import com.redline.coin.R;
import com.redline.coin.util.textview.TextViewSFTextRegular;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TypesAdapter extends RecyclerView.Adapter<TypesAdapter.SimpleViewHolder> implements Filterable {

    private List<String> mDataset;
    private List<String> mDatasetFiltered;
    private Context context;
    private SparseBooleanArray itemSelected;
    private String selectedText;

    public TypesAdapter(Context context, List<String> objects, String selectedText) {
        this.mDataset = objects;
        this.mDatasetFiltered = objects;
        this.context = context;
        itemSelected = new SparseBooleanArray();
        this.selectedText = selectedText;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_types, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, final int position) {
        final String item = mDatasetFiltered.get(position);
        holder.tvTitle.setText(item);

        if (itemSelected != null && itemSelected.size() > 0) {
            if (itemSelected.get(position)) {
                holder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                holder.imgChecked.setVisibility(View.VISIBLE);
            } else {
                holder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.black));
                holder.imgChecked.setVisibility(View.GONE);
            }
        } else {
            if (!TextUtils.isEmpty(selectedText)) {
                String[] webId = item.split("-");
                if (webId.length == 1) { // without "-" string
                    if (selectedText.equalsIgnoreCase(webId[0].trim())) {
                        holder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                        holder.imgChecked.setVisibility(View.VISIBLE);
                    } else {
                        holder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.black));
                        holder.imgChecked.setVisibility(View.GONE);
                    }
                } else {
                    if (selectedText.equalsIgnoreCase(webId[1].trim())) {
                        holder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                        holder.imgChecked.setVisibility(View.VISIBLE);
                    } else {
                        holder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.black));
                        holder.imgChecked.setVisibility(View.GONE);
                    }
                }
            } else {
                holder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.black));
                holder.imgChecked.setVisibility(View.GONE);
            }
        }

        holder.rlView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearSelected();
                itemSelected.put(position, true);
                notifyDataSetChanged();
            }
        });
    }

    private void clearSelected() {
        selectedText = "";
        itemSelected.clear();
    }

    private void selectFirst() {
        itemSelected.put(0, true);
    }

    public String getSelectedItem() {
        if (mDatasetFiltered != null && mDatasetFiltered.size() > 0) {
            for (int i = 0; i < mDatasetFiltered.size(); i++) {
                if (i == itemSelected.keyAt(0)) {
                    return mDatasetFiltered.get(i);
                }
            }
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return mDatasetFiltered != null ? mDatasetFiltered.size() : 0;
    }

    public List<String> getData() {
        return mDatasetFiltered;
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title)
        TextViewSFTextRegular tvTitle;
        @BindView(R.id.img_checked)
        ImageView imgChecked;
        @BindView(R.id.rl_view)
        View rlView;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mDatasetFiltered = mDataset;
                } else {
                    List<String> filteredList = new ArrayList<>();
                    for (String row : mDataset) {
                        if (row.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    mDatasetFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mDatasetFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mDatasetFiltered = (List<String>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
