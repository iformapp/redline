package com.redline.coin.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.redline.coin.R;
import com.redline.coin.model.TopWebsite;
import com.redline.coin.ui.TopWebsitesActivity;
import com.redline.coin.util.Constants;
import com.redline.coin.util.Utils;
import com.redline.coin.util.textview.TextViewSFTextBold;
import com.redline.coin.util.textview.TextViewSFTextLight;
import com.redline.coin.util.textview.TextViewSFTextMedium;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TopWebsiteAdapter extends RecyclerView.Adapter<TopWebsiteAdapter.SimpleViewHolder> {

    private Context mContext;
    private List<TopWebsite.Data> signalsData;

    public TopWebsiteAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_websites, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SimpleViewHolder holder, final int position) {
        TopWebsite.Data data = signalsData.get(position);
        holder.tvNo.setText((position + 1) + "");
        Utils.loadImage(mContext, Constants.IMAGE_URL_WEBSITE + data.imgUrl, holder.imgLogo);
        holder.tvCoinName.setText(data.name);
        holder.tvUrl.setText(data.url);
    }

    @Override
    public int getItemCount() {
        return signalsData != null ? signalsData.size() : 0;
    }

    public void doRefresh(List<TopWebsite.Data> coinData) {
        this.signalsData = coinData;
        notifyDataSetChanged();
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_no)
        TextViewSFTextBold tvNo;
        @BindView(R.id.img_logo)
        ImageView imgLogo;
        @BindView(R.id.tv_coin_name)
        TextViewSFTextMedium tvCoinName;
        @BindView(R.id.tv_url)
        TextViewSFTextLight tvUrl;

        SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((TopWebsitesActivity) mContext).onItemClick(getAdapterPosition());
                }
            });
        }
    }
}
