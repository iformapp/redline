package com.redline.coin.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.redline.coin.R;
import com.redline.coin.model.Signals;
import com.redline.coin.ui.AlertDetailsActivity;
import com.redline.coin.util.Constants;
import com.redline.coin.util.Constants.Currency;
import com.redline.coin.util.Utils;
import com.redline.coin.util.button.ButtonSFTextRegular;
import com.redline.coin.util.textview.TextViewSFTextBold;
import com.redline.coin.util.textview.TextViewSFTextRegular;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SignalAdapter extends RecyclerView.Adapter<SignalAdapter.SimpleViewHolder> {

    private Context mContext;
    private List<Signals.Data> signalsData;

    public SignalAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_signal, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SimpleViewHolder holder, final int position) {
        Signals.Data data = signalsData.get(position);
        holder.tvNo.setText((position + 1) + "");
        holder.tvNo.setVisibility(View.GONE);
        Utils.loadImage(mContext, Constants.IMAGE_URL + data.imgUrl, holder.imgLogo);
        holder.tvCoinName.setText(data.coinName);
        holder.tvUpdateTime.setText(Utils.changeDateFormat("yyyy-MM-dd hh:mm:ss", "MMM dd, yyyy", data.dTimestamp));

        if (data.status.equals(Constants.CLOSE_STATUS)) {
            holder.lablePriceNow.setText(mContext.getString(R.string.sell_target));
            holder.lableSellTarget.setText(mContext.getString(R.string.gain));
            holder.tvPriceNow.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            String htmltext = "<big>" + Utils.numberFormatPercentage(data.gainValue) + "%" + "</big>";
            holder.tvSellTarget.setText(Utils.fromHtml(htmltext));

            setTextAndFont(holder.tvBuyTarget, data.buyTarget, data.currency);
            setTextAndFont(holder.tvPriceNow, data.sellTarget, data.currency);

            setButton(holder.btnBuyStatus, R.drawable.closesignal_button_bg, mContext.getString(R.string.signal_closed));
        } else {
            holder.lablePriceNow.setText(mContext.getString(R.string.price_now));
            holder.lableSellTarget.setText(mContext.getString(R.string.sell_target));
            holder.tvPriceNow.setTextColor(ContextCompat.getColor(mContext, R.color.black));

            setTextAndFont(holder.tvBuyTarget, data.buyTarget, data.currency);
            setTextAndFont(holder.tvPriceNow, data.curruntValue, data.currency);
            setTextAndFont(holder.tvSellTarget, data.sellTarget, data.currency);

            switch (data.statusBuyornot) {
                case "0":
                    setButton(holder.btnBuyStatus, R.drawable.red_button_bg, mContext.getString(R.string.strong_don_t_buy));
                    break;
                case "1":
                    setButton(holder.btnBuyStatus, R.drawable.red_button_bg, mContext.getString(R.string.don_t_buy));
                    break;
                case "2":
                    setButton(holder.btnBuyStatus, R.drawable.blue_button_bg, mContext.getString(R.string.hold));
                    break;
                case "3":
                    setButton(holder.btnBuyStatus, R.drawable.green_button_bg, mContext.getString(R.string.buy));
                    break;
                case "4":
                    setButton(holder.btnBuyStatus, R.drawable.green_button_bg, mContext.getString(R.string.strong_buy));
                    break;
            }
        }
    }

    private void setTextAndFont(TextView textView, String text, String currency) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (c == '0' || c == '.') {
                stringBuilder.append(String.valueOf(c));
            } else {
                break;
            }
        }
        String zeroText = stringBuilder.toString();
        String otherText = text.substring(zeroText.length());
        String htmltext;
        if (currency.equals(Currency.USD.getValue())) {
            htmltext = "<big>" + "$" + text + "</big>";
        } else {
            htmltext = "<small>" + zeroText + "</small><big>" + otherText + " " + currency + "</big>";
        }
        textView.setText(Utils.fromHtml(htmltext));
    }

    private void setButton(Button btnBuyStatus, int drawable, String text) {
        btnBuyStatus.setBackground(ContextCompat.getDrawable(mContext, drawable));
        btnBuyStatus.setText(text);
    }

    @Override
    public int getItemCount() {
        return signalsData != null ? signalsData.size() : 0;
    }

    public void doRefresh(List<Signals.Data> coinData) {
        this.signalsData = coinData;
        notifyDataSetChanged();
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_logo)
        ImageView imgLogo;
        @BindView(R.id.tv_coin_name)
        TextViewSFTextRegular tvCoinName;
        @BindView(R.id.tv_update_time)
        TextViewSFTextRegular tvUpdateTime;
        @BindView(R.id.btn_buy_status)
        ButtonSFTextRegular btnBuyStatus;
        @BindView(R.id.tv_buy_target)
        TextViewSFTextBold tvBuyTarget;
        @BindView(R.id.tv_price_now)
        TextViewSFTextBold tvPriceNow;
        @BindView(R.id.tv_sell_target)
        TextViewSFTextBold tvSellTarget;
        @BindView(R.id.lable_price_now)
        TextViewSFTextRegular lablePriceNow;
        @BindView(R.id.lable_sell_target)
        TextViewSFTextRegular lableSellTarget;
        @BindView(R.id.tv_no)
        TextViewSFTextBold tvNo;

        SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(mContext, AlertDetailsActivity.class);
                    i.putExtra(Constants.SIGNAL_ID, signalsData.get(getAdapterPosition()).signalId);
                    mContext.startActivity(i);
                }
            });
        }
    }
}
