package com.redline.coin.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.redline.coin.ui.BaseActivity;
import com.redline.coin.R;
import com.redline.coin.model.CoinInfoModel;
import com.redline.coin.util.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class InfoPagerAdapter extends PagerAdapter {

    @BindView(R.id.img_logo)
    ImageView imgLogo;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_subtitle)
    TextView tvSubtitle;
    @BindView(R.id.tv_desciption)
    TextView tvDesciption;
    @BindView(R.id.tv_disclaimer)
    TextView tvDisclaimer;
    @BindView(R.id.ll_info_status)
    LinearLayout llInfoStatus;

    private Context mContext;
    private ArrayList<CoinInfoModel> infoData;

    public InfoPagerAdapter(Context mContext, ArrayList<CoinInfoModel> infoData) {
        this.mContext = mContext;
        this.infoData = infoData;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup collection, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup v = (ViewGroup) inflater.inflate(R.layout.item_pager_info, collection, false);
        ButterKnife.bind(this, v);

        CoinInfoModel data = infoData.get(position);
        if (position == 0) {
            tvDesciption.setVisibility(View.VISIBLE);
            llInfoStatus.setVisibility(View.GONE);
            SpannableStringBuilder ss = Utils.getBoldString(mContext, data.description, data.fonts, data.colorList, data.words);

            if (!TextUtils.isEmpty(data.fullTextFont)) {
                Typeface tf = Typeface.createFromAsset(mContext.getAssets(), data.fullTextFont);
                tvDesciption.setTypeface(tf);
            }
            tvDesciption.setText(ss);
        } else {
            tvDesciption.setVisibility(View.GONE);
            llInfoStatus.setVisibility(View.VISIBLE);
            Utils.loadImage(mContext, data.drawable, imgLogo);
            tvTitle.setText(data.title);
            tvSubtitle.setText(data.subtitle);
        }
        tvDisclaimer.setPaintFlags(tvDisclaimer.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        collection.addView(v);
        return v;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup collection, int position, @NonNull Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return infoData != null ? infoData.size() : 0;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

    @OnClick({R.id.tv_disclaimer})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_disclaimer:
                ((BaseActivity) mContext).showDisclaimerDialog();
                break;
        }
    }
}