package com.redline.coin.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.redline.coin.R;
import com.redline.coin.util.textview.TextViewSFTextRegular;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DialogListAdapter extends RecyclerView.Adapter<DialogListAdapter.SimpleViewHolder> {

    private List<String> mDataset;
    private Context context;
    private OnItemSelect onItemSelect;
    private int selectedPosition = 0;

    public DialogListAdapter(Context context, List<String> objects, int selectedPosition, OnItemSelect onItemSelect) {
        this.mDataset = objects;
        this.context = context;
        this.onItemSelect = onItemSelect;
        this.selectedPosition = selectedPosition;
    }

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_types, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SimpleViewHolder holder, final int position) {
        final String item = mDataset.get(position);
        holder.tvTitle.setText(item);

        if (selectedPosition == position) {
            holder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            holder.imgChecked.setImageResource(R.drawable.radio_selected);
        } else {
            holder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.black));
            holder.imgChecked.setImageResource(R.drawable.radio_unselect);
        }
    }

    @Override
    public int getItemCount() {
        return mDataset != null ? mDataset.size() : 0;
    }

    public List<String> getData() {
        return mDataset;
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title)
        TextViewSFTextRegular tvTitle;
        @BindView(R.id.img_checked)
        ImageView imgChecked;
        @BindView(R.id.rl_view)
        View rlView;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemSelect != null)
                        onItemSelect.itemSelect(getAdapterPosition());
                }
            });
        }
    }

    public interface OnItemSelect {
        public void itemSelect(int position);
    }
}
