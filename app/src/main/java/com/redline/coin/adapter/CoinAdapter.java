package com.redline.coin.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.redline.coin.R;
import com.redline.coin.model.Coin;
import com.redline.coin.model.CoinInfoModel;
import com.redline.coin.ui.BaseActivity;
import com.redline.coin.util.Constants;
import com.redline.coin.util.Utils;
import com.redline.coin.util.button.ButtonSFTextRegular;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CoinAdapter extends RecyclerView.Adapter<CoinAdapter.SimpleViewHolder> {

    private Context mContext;
    private List<Coin.Data> coinData;
    private OnTimeChangeListner onTimeChangeListner;
    private boolean isFullName = false;
    private ArrayList<CoinInfoModel> coinInfoModelArrayList;
    private BaseActivity baseActivity;

    public CoinAdapter(Context mContext, OnTimeChangeListner onTimeChangeListner) {
        this.mContext = mContext;
        this.onTimeChangeListner = onTimeChangeListner;
        baseActivity = ((BaseActivity) mContext);
        coinInfoModelArrayList = baseActivity.getDialogInfoData();
    }

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_coin, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SimpleViewHolder holder, final int position) {
        final Coin.Data data = coinData.get(position);
        if (data != null) {
            holder.tvRedcoinSite.setVisibility(View.GONE);
            holder.llCoin.setVisibility(View.VISIBLE);
            //holder.btnViewChart.setVisibility(View.VISIBLE);

            int infoPosition = 0;
            Utils.loadImage(mContext, Constants.IMAGE_URL + data.imgUrl, holder.imgLogo);
            if (isFullName) {
                holder.tvCoinPrice.setVisibility(View.GONE);
                holder.tvCoinName.setText(data.name);
            } else {
                holder.tvCoinPrice.setVisibility(View.VISIBLE);
                holder.tvCoinName.setText(data.webId + " - ");
            }
            holder.tvCoinPrice.setText(Utils.numberFormat(data.values));
            holder.tvUpdateTime.setText("UPDATED: " + data.lastUpdate.toLowerCase() + " ago");
            switch (data.recommendation) {
                case "0":
                    infoPosition = Constants.STRONG_DONT_BUY;
                    setButton(holder.btnBuyStatus, R.drawable.red_button_bg, mContext.getString(R.string.strong_don_t_buy));
                    break;
                case "1":
                    infoPosition = Constants.DONT_BUY;
                    setButton(holder.btnBuyStatus, R.drawable.red_button_bg, mContext.getString(R.string.don_t_buy));
                    break;
                case "2":
                    infoPosition = Constants.HOLD;
                    setButton(holder.btnBuyStatus, R.drawable.blue_button_bg, mContext.getString(R.string.hold));
                    break;
                case "3":
                    infoPosition = Constants.BUY;
                    setButton(holder.btnBuyStatus, R.drawable.green_button_bg, mContext.getString(R.string.buy));
                    break;
                case "4":
                    infoPosition = Constants.STRONG_BUY;
                    setButton(holder.btnBuyStatus, R.drawable.green_button_bg, mContext.getString(R.string.strong_buy));
                    break;
            }

            final int finalInfoPosition = infoPosition;
            holder.btnBuyStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    baseActivity.showInfoDialog(finalInfoPosition, coinInfoModelArrayList);
                }
            });

            holder.llItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isFullName = !isFullName;
                    if (onTimeChangeListner != null)
                        onTimeChangeListner.onTimeChange(isFullName);
                    notifyDataSetChanged();
                }
            });

            holder.btnViewChart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    baseActivity.redirectUsingCustomTab(data.chartUrl);
                }
            });
        } else {
            //holder.btnViewChart.setVisibility(View.GONE);
            holder.tvRedcoinSite.setVisibility(View.VISIBLE);
            holder.llCoin.setVisibility(View.GONE);

            holder.tvRedcoinSite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    baseActivity.redirectUsingCustomTab(Constants.REDLINE_SITE);
                }
            });
        }
    }

    public interface OnTimeChangeListner {
        void onTimeChange(boolean isSecond);
    }

    private void setButton(Button btnBuyStatus, int drawable, String text) {
        btnBuyStatus.setBackground(ContextCompat.getDrawable(mContext, drawable));
        btnBuyStatus.setText(text);
    }

    @Override
    public int getItemCount() {
        return coinData != null ? coinData.size() : 0;
    }

    public void doRefresh(List<Coin.Data> coinData) {
        this.coinData = coinData;
        notifyDataSetChanged();
    }

    static class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_logo)
        ImageView imgLogo;
        @BindView(R.id.tv_coin_name)
        TextView tvCoinName;
        @BindView(R.id.tv_coin_price)
        TextView tvCoinPrice;
        @BindView(R.id.tv_update_time)
        TextView tvUpdateTime;
        @BindView(R.id.btn_buy_status)
        Button btnBuyStatus;
        @BindView(R.id.ll_coin)
        LinearLayout llCoin;
        @BindView(R.id.tv_redcoin_site)
        TextView tvRedcoinSite;
        @BindView(R.id.ll_item)
        LinearLayout llItem;
        @BindView(R.id.btn_view_chart)
        ButtonSFTextRegular btnViewChart;

        SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
