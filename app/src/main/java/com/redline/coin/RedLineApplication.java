package com.redline.coin;

import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

public class RedLineApplication extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);

//        String gaid = Preferences.readString(getApplicationContext(), Constants.GAID, "");
//        if (TextUtils.isEmpty(gaid)) {
//            @SuppressLint("StaticFieldLeak")
//            AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
//                @Override
//                protected String doInBackground(Void... params) {
//                    AdvertisingIdClient.Info idInfo = null;
//                    try {
//                        idInfo = AdvertisingIdClient.getAdvertisingIdInfo(getApplicationContext());
//                    } catch (GooglePlayServicesNotAvailableException e) {
//                        e.printStackTrace();
//                    } catch (GooglePlayServicesRepairableException e) {
//                        e.printStackTrace();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    String advertId = null;
//                    try {
//                        advertId = idInfo.getId();
//                    } catch (NullPointerException e) {
//                        e.printStackTrace();
//                    }
//
//                    return advertId;
//                }
//
//                @Override
//                protected void onPostExecute(String advertId) {
//                    Log.e("GAID", advertId);
//                    Preferences.writeString(getApplicationContext(), Constants.GAID, advertId);
//                }
//
//            };
//            task.execute();
//        }
    }
}
