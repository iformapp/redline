package com.redline.coin.api;

import com.redline.coin.model.Coin;
import com.redline.coin.model.GeneralModel;
import com.redline.coin.model.Language;
import com.redline.coin.model.LikeDislike;
import com.redline.coin.model.Payment;
import com.redline.coin.model.Signals;
import com.redline.coin.model.Subscription;
import com.redline.coin.model.TopWebsite;
import com.redline.coin.model.UserModel;
import com.redline.coin.model.YouTubeChannel;
import com.redline.coin.util.Constants;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiInterface {

    @FormUrlEncoded
    @POST(Constants.LOGIN)
    Call<UserModel> login(@Field("email") String email, @Field("password") String password,
                          @Field("notification_token") String notificationToken, @Field("device_type") int deviceType);

    @FormUrlEncoded
    @POST(Constants.REGISTER)
    Call<UserModel> register(@Field("username") String username, @Field("email") String email,
                             @Field("password") String password, @Field("notification_token") String notificationToken,
                             @Field("device_type") int deviceType);

    @FormUrlEncoded
    @POST(Constants.LOGOUT)
    Call<GeneralModel> logout(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Constants.FORGOT_PASSWORD)
    Call<GeneralModel> forgotPassword(@Field("email") String email);

    @FormUrlEncoded
    @POST(Constants.OTP_RESET_PASSWORD)
    Call<GeneralModel> resetPassword(@Field("email") String email, @Field("otp") String otp,
                                     @Field("password") String password);

    @FormUrlEncoded
    @POST(Constants.CHANGE_PASSWORD)
    Call<GeneralModel> changePassword(@Field("access_token") String accessToken, @Field("old_password") String oldPassword,
                                      @Field("new_password") String newPassword);

    @FormUrlEncoded
    @POST(Constants.COIN_URL)
    Call<Coin> getCoinValue(@Field("status_buyornot") String status, @Field("access_token") String accessToken);

    @FormUrlEncoded
    @POST(Constants.COIN_URL + "/{id}")
    Call<Coin> getCoinValue(@Path("id") int term, @Field("access_token") String accessToken);

    @FormUrlEncoded
    @POST(Constants.SIGNALS_URL)
    Call<Signals> getSignalsByStatus(@Field("status") int status, @Field("access_token") String accessToken);

    @FormUrlEncoded
    @POST(Constants.GET_SIGNAL)
    Call<Signals> getSignal(@Field("signal_id") String signalId, @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Constants.ADD_ALERT)
    Call<GeneralModel> addAlert(@Field("status") int status, @Field("alert_type") int alertType,
                                @Field("coin_id") String coinId, @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Constants.ADD_SUBSCRIPTION)
    Call<GeneralModel> addSubscription(@Field("status") int status, @Field("device_type") int deviceType,
                                       @Field("token") String token, @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Constants.CHECK_SUBSCRIPTION)
    Call<Subscription> checkSubscription(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Constants.GET_ALERT)
    Call<Object> getAlerts(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Constants.CHANGE_SIGNAL_STATUS)
    Call<GeneralModel> changeSignalStatus(@Field("alert_status") int status, @Field("alert_type") String alertType,
                                          @Field("signal_id") String signalId, @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Constants.NEW_SIGNAL_URL)
    Call<GeneralModel> newSignalUpdate(@Field("final_status") int status, @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Constants.ALL_COIN_URL)
    Call<Coin> getAllCoin(@Field("access_token") String accessToken);

    @FormUrlEncoded
    @POST(Constants.ADD_PAYMENT)
    Call<Payment> addPayment(@Field("user_id") String user_id, @Field("type") String type,
                             @Field("duration") String duration, @Field("amount") String amount);

    @FormUrlEncoded
    @POST(Constants.TOP_WEBSITE)
    Call<TopWebsite> getTopWebsite(@Field("location") String location, @Field("coin_id") String coin_id,
                                   @Field("feature") int feature, @Field("search") String search, @Field("access_token") String accessToken);

    @FormUrlEncoded
    @POST(Constants.LIKE_DISLIKE_WEBSITE)
    Call<LikeDislike> likeDislike(@Field("user_id") String user_id, @Field("confirm") int confirm, @Field("website_id") String website_id);

    @FormUrlEncoded
    @POST(Constants.LIKE_DISLIKE_YOUTUBE)
    Call<LikeDislike> likeDislikeYouTube(@Field("user_id") String user_id, @Field("confirm") int confirm, @Field("yid") String yid);

    @GET(Constants.LANGUAGES_URL)
    Call<Language> getLanguages();

    @FormUrlEncoded
    @POST(Constants.YOUTUBE_CHANNEL_URL)
    Call<YouTubeChannel> getYouTubeChannel(@Field("language") String language, @Field("access_token") String accessToken,
                                           @Field("param") String param);
}
