package com.redline.coin.util.justify;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;

import com.redline.coin.util.Constants;

public class JustifySFTextSemiBold extends JustifiedTextView {

    public JustifySFTextSemiBold(Context context) {
        super(context);
        applyCustomFont();
    }

    public JustifySFTextSemiBold(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont();
    }

    public JustifySFTextSemiBold(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont();
    }

    private void applyCustomFont() {
        if (!TextUtils.isEmpty(Constants.SFTEXT_SEMIBOLD)) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), Constants.SFTEXT_SEMIBOLD);
            setTypeface(tf);
        }
    }
}
