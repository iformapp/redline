package com.redline.coin.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Utils {

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(html);
        }
    }

    public static Drawable setTint(Drawable drawable, int color) {
        final Drawable newDrawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(newDrawable, color);
        return newDrawable;
    }

    public static SpannableStringBuilder getBoldString(Context context, String s, String[] fonts, int[] colorList, String[] words) {
        if (TextUtils.isEmpty(s))
            return null;

        SpannableStringBuilder ss = new SpannableStringBuilder(s);
        try {
            for (int i = 0; i < words.length; i++) {
                if (s.contains(words[i])) {
                    if (colorList != null) {
                        ss.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, colorList[i])), s.indexOf(words[i]),
                                s.indexOf(words[i]) + words[i].length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }

                    if (fonts != null && fonts.length > i) {
                        Typeface font = Typeface.createFromAsset(context.getAssets(), fonts[i]);
                        ss.setSpan (new CustomTypefaceSpan("", font), s.indexOf(words[i]), s.indexOf(words[i]) + words[i].length(),
                                Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ss;
    }

    public static SpannableStringBuilder getColorString(Context context, String s, String word, int color) {
        SpannableStringBuilder ss = new SpannableStringBuilder(s);
        ss.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, color)), s.indexOf(word),
                s.indexOf(word) + word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ss;
    }

    public static void hideSoftKeyboard(Activity activity) {
        if (activity == null)
            return;

        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null && activity.getCurrentFocus() != null) {
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static void openSoftKeyboard(Activity activity, View view) {
        if (activity == null)
            return;

        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return model;
        }
        return manufacturer + " " + model;
    }

    public static String numberFormat(String number) {
        try {
            Double d = Double.parseDouble(number);
            DecimalFormat nf = new DecimalFormat(d > 1 ? "0.00" : "0.0000");
            return currencyFormat(nf.format(d));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return number;
    }

    public static String numberFormatPercentage(String number) {
        try {
            Double d = Double.parseDouble(number);
            DecimalFormat nf = new DecimalFormat("0.00");
            return nf.format(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return number;
    }

    private static char[] c = new char[]{'K', 'M', 'B', 'T'};

    public static String bigCountFormat(String number) {
        if (TextUtils.isEmpty(number))
            return "";

        if (Double.parseDouble(number) >= 1000) {
            return getBigCountFormat(Double.parseDouble(number), 0);
        }
        return number;
    }

    private static String getBigCountFormat(double n, int iteration) {
        double d = ((long) n / 100) / 10.0;
        boolean isRound = (d * 10) %10 == 0; //true if the decimal part is equal to 0 (then it's trimmed anyway)
        return (d < 1000? //this determines the class, i.e. 'k', 'm' etc
                ((d > 99.9 || isRound || (!isRound && d > 9.99)? //this decides whether to trim the decimals
                        (int) d * 10 / 10 : d + "" // (int) d * 10 / 10 drops the decimal
                ) + "" + c[iteration])
                : getBigCountFormat(d, iteration+1));
    }

    public static String convertToSuffix(long count) {
        if (count < 1000) return "" + count;
        int exp = (int) (Math.log(count) / Math.log(1000));
        return String.format("%.1f%c",
                count / Math.pow(1000, exp),
                "KMGTPE".charAt(exp - 1));
    }

    public static String numberFormatPercentage(double number) {
        try {
            DecimalFormat nf = new DecimalFormat("0.00");
            return nf.format(number);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return String.valueOf(number);
    }

    private static String currencyFormat(String number) {
        try {
            Double d = Double.parseDouble(number);
            NumberFormat defaultFormat = NumberFormat.getCurrencyInstance(Locale.US);
            return defaultFormat.format(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return number;
    }

    public static void loadImage(Context context, String url, ImageView imageView) {
        if (context != null && imageView != null)
            Glide.with(context).load(url).into(imageView);
    }

    public static void loadImage(Context context, int drawable, ImageView imageView) {
        if (context != null && imageView != null)
            Glide.with(context).load(drawable).into(imageView);
    }

    public static String changeDateFormat(String source, String target, String dateString) {
        SimpleDateFormat input = new SimpleDateFormat(source);
        SimpleDateFormat output = new SimpleDateFormat(target);
        try {
            Date date = input.parse(dateString);
            return output.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateString;
    }

    public static String changeDateFormat(String format, Date date) {
        SimpleDateFormat output = new SimpleDateFormat(format);
        return output.format(date);
    }

    public static String getFileExtFromBytes(File f) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(f);
            byte[] buf = new byte[5]; //max ext size + 1
            fis.read(buf, 0, buf.length);
            StringBuilder builder = new StringBuilder(buf.length);
            for (int i=1;i<buf.length && buf[i] != '\r' && buf[i] != '\n';i++) {
                builder.append((char)buf[i]);
            }
            return builder.toString().toLowerCase();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
