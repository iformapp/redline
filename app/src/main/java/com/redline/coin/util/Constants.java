package com.redline.coin.util;

public class Constants {

    //TEST URL
    //public static final String BASE_URL = "https://test.redlinecoin.com/";

    //LIVE URL
    public static final String BASE_URL = "https://www.redlinecoin.com/";

    private static final String VERSION_URL = "api/index.php/";

    public static final String IMAGE_URL = BASE_URL + "api/img_coins/";
    public static final String IMAGE_URL_WEBSITE = BASE_URL + "img/";
    public static final String LOGIN = VERSION_URL + "Auth/login";
    public static final String REGISTER = VERSION_URL + "Auth/register";
    public static final String FORGOT_PASSWORD = VERSION_URL + "Auth/step1";
    public static final String OTP_RESET_PASSWORD = VERSION_URL + "Auth/step2";
    public static final String LOGOUT = VERSION_URL + "Auth/logout";
    public static final String CHANGE_PASSWORD = VERSION_URL + "Auth/change_password";
    public static final String COIN_URL = VERSION_URL + "coins/getCoinsValue";
    public static final String ALL_COIN_URL = VERSION_URL + "coins/getCoinsAll";
    public static final String SIGNALS_URL = VERSION_URL + "Signals/getSignalsByStatus";
    public static final String GET_ALERT = VERSION_URL + "Alerts/getAlerts";
    public static final String ADD_ALERT = VERSION_URL + "Alerts/addAlert";
    public static final String ADD_SUBSCRIPTION = VERSION_URL + "subscriptions/addSubscription";
    public static final String CHECK_SUBSCRIPTION = VERSION_URL + "Payment/check_payment_subc";
    public static final String ADD_PAYMENT = "payment.php";
    public static final String GET_SIGNAL = VERSION_URL + "Signals/get_single_Signal";
    public static final String CHANGE_SIGNAL_STATUS = VERSION_URL + "Signals/change_signal_status";
    public static final String NEW_SIGNAL_URL = VERSION_URL + "Signals/final_status_change";
    public static final String TOP_WEBSITE = VERSION_URL + "Websites/getWebsites";
    public static final String LIKE_DISLIKE_WEBSITE = VERSION_URL + "Websites/change_status";
    public static final String LIKE_DISLIKE_YOUTUBE = VERSION_URL + "Youtube_channels/change_status";
    public static final String LANGUAGES_URL = VERSION_URL + "Youtube_channels/getlanguages";
    public static final String YOUTUBE_CHANNEL_URL = VERSION_URL + "Youtube_channels/getChannels";
    public static final String REDLINE_SITE = BASE_URL;
    public static final String POLICY = BASE_URL + "policy.html";
    public static final String TERMS_SERVICE = BASE_URL + "terms-of-use.html";
    public static final String CHEAPEST_ESSAY_SITE = "https://www.cheapestessay.com";
    public static final String TASK24_SITE = "https://www.24Task.com";
    public static final String IFORM_SITE = "http://iformapp.com/#/home";

    public static final int TAB_HOME = 0;
    public static final int TAB_INVESTING = 1;
    public static final int TAB_SIGNAL = 2;
    public static final int TAB_NOTIFICATION = 3;
    public static final int TAB_MORE = 4;

    public static final int DEVICE_TYPE = 2; // for android

    public static final String SFTEXT_REGULAR = "SanFranciscoText-Regular.otf";
    public static final String SFTEXT_BOLD = "SanFranciscoText-Bold.otf";
    public static final String SFTEXT_MEDIUM = "SanFranciscoText-Medium.otf";
    public static final String SFTEXT_LIGHT = "SanFranciscoText-Light.otf";
    public static final String SFTEXT_SEMIBOLD = "SanFranciscoText-Semibold.otf";
    public static final String SFDISPLAY_BOLD = "SF-Pro-Display-Bold.otf";
    public static final String SFDISPLAY_REGULAR = "SF-Pro-Display-Regular.otf";
    public static final String SFDISPLAY_MEDIUM = "SF-Pro-Display-Medium.otf";

    public static final String IS_FIRST = "isFirst";
    public static final String FCM_TOKEN = "fcm_token";
    public static final String FROM_LOGIN = "from login";
    public static final String IS_LOGIN = "isLogin";
    public static final String GAID = "GAID";
    public static final String USER_DATA = "user_data";
    public static final String SCREEN_NAME = "screen_name";
    public static final String COIN_DATA = "coindata";
    public static final String JSON_DATA = "jsondata";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String SIGNAL_ID = "signal_id";
    public static final String OPEN_STATUS = "2";
    public static final String CLOSE_STATUS = "3";
    public static final String NOTIFICATION_BUYORNOT = "buy or not";
    public static final String TYPE_CURRENCY = "type_currency";
    public static final String TYPE_LOCATION = "type_location";

    public static final int ALL = 0;
    public static final int STRONG_BUY = 1;
    public static final int BUY = 2;
    public static final int HOLD = 3;
    public static final int DONT_BUY = 4;
    public static final int STRONG_DONT_BUY = 5;

    public static final String REDLINE_COIN = "Redline Coin";
    public static final String S_BUY = "Buy";
    public static final String S_HOLD = "Hold";
    public static final String S_DONT_BUY = "Don’t Buy";

    public static final String HIGH = "3";
    public static final String MEDIUM = "1";
    public static final String LOW = "2";

    public static final int OPEN_SIGNAL = 2;
    public static final int CLOSE_SIGNAL = 3;

    public static final int SHORT_TERM = 0;
    public static final int LONG_TERM = 1;

    public static final String INVESTING_SUBSCRIPTION = "investing_subscription";
    public static final String SIGNAL_SUBSCRIPTION = "signal_subscription";
    public static final String MONTHLY = "month";
    public static final String YEARLY = "year";
    public static final String TOP_INV = "top-inv";
    public static final String SIGNAL = "signal";
    public static final String PAYPAL_URL = "paypal_url";

    public static final String SIGNAL_YEARLY_PRICE = "89.00";
    public static final String SIGNAL_MONTHLY_PRICE = "9.00";
    public static final String INVESTING_YEARLY_PRICE = "85.00";
    public static final String INVESTING_MONTHLY_PRICE = "18.00";

    public enum StatusType {
        STRONG_DONT_BUY(0, "strong_dont_buy"),
        DONT_BUY(1, "dont_buy"),
        HOLD(2, "hold"),
        BUY(3, "buy"),
        STRONG_BUY(4, "strong_buy"),
        CLOSE(5, "close_status");

        int value;
        String status;

        StatusType(int value, String status) {
            this.value = value;
            this.status = status;
        }

        public int getValue() {
            return value;
        }

        public String getStatus() {
            return status;
        }
    }

    public enum Currency {
        USD("USD"),
        BTC("BTC");

        String value;

        Currency(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
