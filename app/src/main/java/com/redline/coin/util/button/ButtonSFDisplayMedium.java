package com.redline.coin.util.button;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.Button;

import com.redline.coin.util.Constants;

public class ButtonSFDisplayMedium extends Button {

    public ButtonSFDisplayMedium(Context context) {
        super(context);
        applyCustomFont();
    }

    public ButtonSFDisplayMedium(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont();
    }

    public ButtonSFDisplayMedium(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont();
    }

    private void applyCustomFont() {
        if (!TextUtils.isEmpty(Constants.SFDISPLAY_MEDIUM)) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), Constants.SFDISPLAY_MEDIUM);
            setTypeface(tf);
        }
    }
}
