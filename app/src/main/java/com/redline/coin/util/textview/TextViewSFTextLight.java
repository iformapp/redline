package com.redline.coin.util.textview;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

import com.redline.coin.util.Constants;

public class TextViewSFTextLight extends TextView {

    public TextViewSFTextLight(Context context) {
        super(context);
        applyCustomFont();
    }

    public TextViewSFTextLight(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont();
    }

    public TextViewSFTextLight(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont();
    }

    private void applyCustomFont() {
        if (!TextUtils.isEmpty(Constants.SFTEXT_LIGHT)) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), Constants.SFTEXT_LIGHT);
            setTypeface(tf);
        }
    }
}
