package com.redline.coin.util.button;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.Button;

import com.redline.coin.util.Constants;

public class ButtonSFTextMedium extends Button {

    public ButtonSFTextMedium(Context context) {
        super(context);
        applyCustomFont();
    }

    public ButtonSFTextMedium(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont();
    }

    public ButtonSFTextMedium(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont();
    }

    private void applyCustomFont() {
        if (!TextUtils.isEmpty(Constants.SFTEXT_MEDIUM)) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), Constants.SFTEXT_MEDIUM);
            setTypeface(tf);
        }
    }
}
