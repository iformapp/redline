package com.redline.coin.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import com.redline.coin.R;
import com.redline.coin.adapter.OpenSignalAdapter;
import com.redline.coin.model.GeneralModel;
import com.redline.coin.model.Signals;
import com.redline.coin.model.UserModel;
import com.redline.coin.ui.SubscriptionActivity;
import com.redline.coin.util.Constants;
import com.redline.coin.util.Preferences;
import com.redline.coin.util.textview.TextViewSFTextRegular;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignalFragment extends BaseFragment {

    Unbinder unbinder;
    @BindView(R.id.rv_open_signal)
    RecyclerView rvOpenSignal;
    @BindView(R.id.tv_new_signal)
    TextViewSFTextRegular tvNewSignal;
    @BindView(R.id.toggle_new_signal)
    ToggleButton toggleNewSignal;
    @BindView(R.id.card_subscribe)
    CardView cardSubscribe;

    private OpenSignalAdapter mAdapter;
    private List<Signals.Data> signalsList;
    private boolean isRefresh = false;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_signal, container, false);
        unbinder = ButterKnife.bind(this, v);

        rvOpenSignal.setLayoutManager(new LinearLayoutManager(activity));

        if (activity.isSignalSubscribed()) {
            getSignalByStatus();
        } else {
            cardSubscribe.setVisibility(View.VISIBLE);
        }

        toggleNewSignal.setChecked(Preferences.getUserData(activity).newSignal.equals("1"));

        toggleNewSignal.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                newSignalUpdate(isChecked ? 1 : 0);
            }
        });

        rvOpenSignal.setNestedScrollingEnabled(false);
        return v;
    }

    public void newSignalUpdate(final int status) {
        if (!activity.isNetworkConnected()) {
            return;
        }

        activity.showProgress();

        Call<GeneralModel> call = activity.getService().newSignalUpdate(status, activity.getUserId());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (activity.checkStatus(response.body())) {
                    UserModel.Data userModel = Preferences.getUserData(activity);
                    userModel.newSignal = String.valueOf(status);
                    Preferences.saveUserData(activity, userModel);
                }
                activity.hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                activity.failureError("new signal alert failed");
            }
        });
    }

    public void getSignalByStatus() {
        if (!activity.isNetworkConnected()) {
            return;
        }

        activity.showProgress();

        Call<Signals> call = activity.getService().getSignalsByStatus(2, activity.getAccessToken()); // 2 for open signals
        call.enqueue(new Callback<Signals>() {
            @Override
            public void onResponse(Call<Signals> call, Response<Signals> response) {
                Signals signals = response.body();
                if (signals != null) {
                    if (activity.checkStatus(signals)) {
                        signalsList = new ArrayList<>();
                        signalsList = signals.data;
                        setAdapter();
                    } else {
                        isRefresh = true;
                    }
                }

                activity.hideProgress();
            }

            @Override
            public void onFailure(Call<Signals> call, Throwable t) {
                activity.failureError("get signal data failed");
            }
        });
    }

    public void setAdapter() {
        if (signalsList != null && signalsList.size() > 0) {
            if (mAdapter == null) {
                mAdapter = new OpenSignalAdapter(activity);
            }

            mAdapter.doRefresh(signalsList);

            if (rvOpenSignal != null && rvOpenSignal.getAdapter() == null) {
                rvOpenSignal.setAdapter(mAdapter);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isRefresh) {
            isRefresh = false;
            if (activity.isSignalSubscribed()) {
                getSignalByStatus();
            } else {
                cardSubscribe.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btn_subscribing)
    public void onViewClicked() {
        Intent i = new Intent(activity, SubscriptionActivity.class);
        i.putExtra(Constants.INVESTING_SUBSCRIPTION, false);
        startActivity(i);
    }
}
