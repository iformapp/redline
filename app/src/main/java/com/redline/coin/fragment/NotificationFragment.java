package com.redline.coin.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import com.google.gson.Gson;
import com.redline.coin.R;
import com.redline.coin.model.Coin;
import com.redline.coin.model.GeneralModel;
import com.redline.coin.util.Constants;
import com.redline.coin.util.Constants.StatusType;
import com.redline.coin.util.Utils;
import com.redline.coin.util.textview.TextViewSFTextRegular;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationFragment extends BaseFragment {

    @BindView(R.id.tv_strong_buy)
    TextViewSFTextRegular tvStrongBuy;
    @BindView(R.id.toggle_strong_buy)
    ToggleButton toggleStrongBuy;
    @BindView(R.id.tv_buy)
    TextViewSFTextRegular tvBuy;
    @BindView(R.id.toggle_buy)
    ToggleButton toggleBuy;
    @BindView(R.id.tv_dont_buy)
    TextViewSFTextRegular tvDontBuy;
    @BindView(R.id.toggle_dont_buy)
    ToggleButton toggleDontBuy;
    @BindView(R.id.tv_strong_dontbuy)
    TextViewSFTextRegular tvStrongDontbuy;
    @BindView(R.id.toggle_strong_dontbuy)
    ToggleButton toggleStrongDontbuy;
    @BindView(R.id.toggle_hold)
    ToggleButton toggleHold;
    Unbinder unbinder;

    private Coin.Data coinData;
    private JSONObject jsonObject;

    public static NotificationFragment newInstanace(String singleData, JSONObject jsonData) {
        NotificationFragment fragment = new NotificationFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.COIN_DATA, singleData);
        if (jsonData != null)
            bundle.putString(Constants.JSON_DATA, jsonData.toString());
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_notification, container, false);
        unbinder = ButterKnife.bind(this, v);

        if (getArguments() != null) {
            coinData = new Gson().fromJson(getArguments().getString(Constants.COIN_DATA), Coin.Data.class);
            try {
                String json = getArguments().getString(Constants.JSON_DATA);
                if (!TextUtils.isEmpty(json))
                    jsonObject = new JSONObject(getArguments().getString(Constants.JSON_DATA));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (jsonObject != null) {
            try {
                Iterator<String> iter = jsonObject.keys();
                while (iter.hasNext()) {
                    String key = iter.next();
                    if (key.equalsIgnoreCase(coinData.webId)) {
                        if (jsonObject.get(key) instanceof JSONObject) {
                            JSONObject coin = jsonObject.getJSONObject(key);
                            Iterator<String> iterCoin = coin.keys();
                            while (iterCoin.hasNext()) {
                                String indexKey = iterCoin.next();
                                setToggle(Integer.parseInt(coin.getString(indexKey)));
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        tvStrongBuy.setText(Utils.getColorString(getActivity(), getString(R.string.strong_buy), "Buy", R.color.light_green));
        tvDontBuy.setText(Utils.getColorString(getActivity(), getString(R.string.don_t_buy), "Don't", R.color.light_red));
        tvStrongDontbuy.setText(Utils.getColorString(getActivity(), getString(R.string.strong_don_t_buy), "Don't", R.color.light_red));

        toggleStrongBuy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                addAlerts(isChecked ? 1 : 0, StatusType.STRONG_BUY.getValue());
            }
        });

        toggleBuy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                addAlerts(isChecked ? 1 : 0, StatusType.BUY.getValue());
            }
        });

        toggleHold.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                addAlerts(isChecked ? 1 : 0, StatusType.HOLD.getValue());
            }
        });

        toggleDontBuy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                addAlerts(isChecked ? 1 : 0, StatusType.DONT_BUY.getValue());
            }
        });

        toggleStrongDontbuy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                addAlerts(isChecked ? 1 : 0, StatusType.STRONG_DONT_BUY.getValue());
            }
        });
        return v;
    }

    public void setToggle(int position) {
        StatusType statusType = StatusType.values()[position];
        switch (statusType) {
            case BUY:
                toggleBuy.setChecked(true);
                break;
            case DONT_BUY:
                toggleDontBuy.setChecked(true);
                break;
            case HOLD:
                toggleHold.setChecked(true);
                break;
            case STRONG_BUY:
                toggleStrongBuy.setChecked(true);
                break;
            case STRONG_DONT_BUY:
                toggleStrongDontbuy.setChecked(true);
                break;
        }
    }

    public void addAlerts(int status, final int alertType) {
        if (!activity.isNetworkConnected()) {
            return;
        }

        activity.showProgress();

        Call<GeneralModel> call = activity.getService().addAlert(status, alertType, coinData.webId, activity.getUserId());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (activity.checkStatus(response.body())) {
                }
                activity.hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                activity.failureError("add alert failed");
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
