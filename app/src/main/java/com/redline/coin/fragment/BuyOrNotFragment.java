package com.redline.coin.fragment;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.redline.coin.R;
import com.redline.coin.model.Coin;
import com.redline.coin.util.Constants;
import com.redline.coin.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BuyOrNotFragment extends BaseFragment {

    @BindView(R.id.tabsAlert)
    TabLayout tabLayout;
    @BindView(R.id.viewPagerAlert)
    ViewPager viewPager;

    private List<Coin.Data> coinData;
    private JSONObject jsonData;
    private boolean isRefresh = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_buy_or_not, container, false);
        ButterKnife.bind(this, v);

        getCoinValue();
        return v;
    }

    public void getCoinValue() {
        if (!activity.isNetworkConnected()) {
            return;
        }

        //activity.showProgress();

        Call<Coin> call = activity.getService().getCoinValue("1", activity.getAccessToken());
        call.enqueue(new Callback<Coin>() {
            @Override
            public void onResponse(Call<Coin> call, Response<Coin> response) {
                Coin coin = response.body();
                if (coin != null && activity.checkStatus(coin)) {
                    coinData = new ArrayList<>();
                    coinData = coin.data;
                    getAlerts();
                } else {
                    isRefresh = true;
                }
            }

            @Override
            public void onFailure(Call<Coin> call, Throwable t) {
                activity.failureError("get coin failed");
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isRefresh) {
            isRefresh = false;
            getCoinValue();
        }
    }

    public void getAlerts() {
        if (!activity.isNetworkConnected()) {
            return;
        }

        Call<Object> call = activity.getService().getAlerts(activity.getUserId());
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(@NonNull Call<Object> call, @NonNull Response<Object> response) {
                String jsonResponse = new Gson().toJson(response.body());
                if (!TextUtils.isEmpty(jsonResponse)) {
                    Log.e("Response : ", jsonResponse);
                    JSONObject main = null;
                    try {
                        main = new JSONObject(jsonResponse);
                        if (main.getString("flag").equals("1") || main.getString("flag").equals("1.0")) {
                            jsonData = main.getJSONObject("data");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                initTab();
            }

            @Override
            public void onFailure(@NonNull Call<Object> call, @NonNull Throwable t) {
                activity.failureError("get alert failed");
            }
        });
    }

    public void initTab() {
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                if (view != null) {
                    TextView text = view.findViewById(R.id.tab);
                    ImageView img = view.findViewById(R.id.tab_img);
                    img.setAlpha(1.0f);
                    Typeface tf = Typeface.createFromAsset(activity.getAssets(), Constants.SFTEXT_BOLD);
                    text.setTypeface(tf);
                    text.setTextColor(Color.BLACK);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                if (view != null) {
                    TextView text = view.findViewById(R.id.tab);
                    ImageView img = view.findViewById(R.id.tab_img);
                    img.setAlpha(0.5f);
                    Typeface tf = Typeface.createFromAsset(activity.getAssets(), Constants.SFTEXT_REGULAR);
                    text.setTypeface(tf);
                    text.setTextColor(ContextCompat.getColor(activity, R.color.android_text_gray));
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setupTabIcons() {
        for (int i = 0; i < coinData.size(); i++) {
            Coin.Data data = coinData.get(i);
            View view = LayoutInflater.from(activity).inflate(R.layout.custom_tab_alert, null);
            TextView tvTabOne = view.findViewById(R.id.tab);
            ImageView imgTabOne = view.findViewById(R.id.tab_img);
            tvTabOne.setText(data.webId);
            Utils.loadImage(activity, Constants.IMAGE_URL + data.imgUrl, imgTabOne);
            if (i == 0) {
                Typeface tf = Typeface.createFromAsset(activity.getAssets(), Constants.SFTEXT_BOLD);
                tvTabOne.setTypeface(tf);
                tvTabOne.setTextColor(Color.BLACK);
                imgTabOne.setAlpha(1.0f);
            } else {
                imgTabOne.setAlpha(0.5f);
            }
            tabLayout.getTabAt(i).setCustomView(view);
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(activity.getSupportFragmentManager());
        for (int i = 0; i < coinData.size(); i++) {
            String data = new Gson().toJson(coinData.get(i));
            adapter.addFrag(NotificationFragment.newInstanace(data, jsonData), coinData.get(i).webId);
        }
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(coinData.size() - 1);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }
}
