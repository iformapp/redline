package com.redline.coin.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TopWebsite extends GeneralModel {

    @Expose
    @SerializedName("data")
    public List<Data> data;

    public static class Data {
        @Expose
        @SerializedName("dislikes")
        public int dislikes;
        @Expose
        @SerializedName("likes")
        public int likes;
        @Expose
        @SerializedName("uid_confirmation")
        public String uidConfirmation;
        @Expose
        @SerializedName("status")
        public String status;
        @Expose
        @SerializedName("rank")
        public String rank;
        @Expose
        @SerializedName("feature")
        public String feature;
        @Expose
        @SerializedName("name")
        public String name;
        @Expose
        @SerializedName("referral_url")
        public String referralUrl;
        @Expose
        @SerializedName("img_url")
        public String imgUrl;
        @Expose
        @SerializedName("url")
        public String url;
        @Expose
        @SerializedName("id")
        public String id;
    }
}
