package com.redline.coin.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LikeDislike extends GeneralModel {

    @Expose
    @SerializedName("data")
    public List<Data> data;

    public static class Data {
        @Expose
        @SerializedName("dislikes")
        public int dislikes;
        @Expose
        @SerializedName("likes")
        public int likes;
    }
}
