package com.redline.coin.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payment extends GeneralModel {

    @Expose
    @SerializedName("url")
    public String url;
}
