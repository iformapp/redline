package com.redline.coin.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserModel extends GeneralModel {

    @Expose
    @SerializedName("data")
    public Data data;

    public static class Data {
        @Expose
        @SerializedName("email")
        public String email;
        @Expose
        @SerializedName("access_token")
        public String accessToken;
        @Expose
        @SerializedName("username")
        public String username;
        @Expose
        @SerializedName("user_id")
        public String userId;
        @Expose
        @SerializedName("new_signal")
        public String newSignal;
    }
}
