package com.redline.coin.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GeneralModel implements Serializable {

    @Expose
    @SerializedName("msg")
    public String msg;
    @Expose
    @SerializedName("flag")
    public int flag;
}
