package com.redline.coin.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class YouTubeChannel extends GeneralModel {

    @Expose
    @SerializedName("data")
    public List<Data> data;

    public static class Data {
        @Expose
        @SerializedName("dislikes")
        public int dislikes;
        @Expose
        @SerializedName("yid_confirmation")
        public String yidConfirmation;
        @Expose
        @SerializedName("d_added")
        public String dAdded;
        @Expose
        @SerializedName("lang")
        public String lang;
        @Expose
        @SerializedName("likes")
        public int likes;
        @Expose
        @SerializedName("popularity")
        public String popularity;
        @Expose
        @SerializedName("videoCount")
        public String videocount;
        @Expose
        @SerializedName("subscriberCount")
        public String subscribercount;
        @Expose
        @SerializedName("viewCount")
        public String viewcount;
        @Expose
        @SerializedName("description")
        public String description;
        @Expose
        @SerializedName("title")
        public String title;
        @Expose
        @SerializedName("channel_id")
        public String channelId;
        @Expose
        @SerializedName("id")
        public String id;
    }
}
