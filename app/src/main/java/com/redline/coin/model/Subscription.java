package com.redline.coin.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Subscription extends GeneralModel {

    @Expose
    @SerializedName("data")
    public Data data;

    public static class Data {
        @Expose
        @SerializedName("top-inv")
        public int topInv;
        @Expose
        @SerializedName("signal")
        public int signal;
    }
}
