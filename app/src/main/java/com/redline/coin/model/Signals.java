package com.redline.coin.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Signals extends GeneralModel {

    @Expose
    @SerializedName("data")
    public List<Data> data;

    public static class Data {
        @Expose
        @SerializedName("strong_dont_buy")
        public String strongDontBuy;
        @Expose
        @SerializedName("hold")
        public String hold;
        @Expose
        @SerializedName("dont_buy")
        public String dontBuy;
        @Expose
        @SerializedName("strong_buy")
        public String strongBuy;
        @Expose
        @SerializedName("buy")
        public String buy;
        @Expose
        @SerializedName("close_status")
        public String closeStatus;
        @Expose
        @SerializedName("reach_time")
        public String reachTime;
        @Expose
        @SerializedName("send_time")
        public String sendTime;
        @Expose
        @SerializedName("gain_value")
        public String gainValue;
        @Expose
        @SerializedName("d_timestamp")
        public String dTimestamp;
        @Expose
        @SerializedName("updated_at")
        public String updatedAt;
        @Expose
        @SerializedName("remarks")
        public String remarks;
        @Expose
        @SerializedName("status_buyornot")
        public String statusBuyornot;
        @Expose
        @SerializedName("exchange_id")
        public String exchangeId;
        @Expose
        @SerializedName("stop_loss")
        public String stopLoss;
        @Expose
        @SerializedName("risk_type")
        public String riskType;
        @Expose
        @SerializedName("sell_target")
        public String sellTarget;
        @Expose
        @SerializedName("buy_target")
        public String buyTarget;
        @Expose
        @SerializedName("currency")
        public String currency;
        @Expose
        @SerializedName("img_url")
        public String imgUrl;
        @Expose
        @SerializedName("coin_name")
        public String coinName;
        @Expose
        @SerializedName("currunt_value")
        public String curruntValue;
        @Expose
        @SerializedName("status")
        public String status;
        @Expose
        @SerializedName("signal_id")
        public String signalId;
        @Expose
        @SerializedName("web_id")
        public String webId;
    }
}
