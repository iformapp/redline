package com.redline.coin.model;

public class CoinInfoModel {

    public String description;
    public String title;
    public String subtitle;
    public int drawable;
    public int[] colorList;
    public String[] fonts;
    public String[] words;
    public String fullTextFont;

    public CoinInfoModel(String description, String title, String subtitle, int drawable) {
        this.description = description;
        this.title = title;
        this.subtitle = subtitle;
        this.drawable = drawable;
    }

    public CoinInfoModel(String description, String fullTextFont, String[] fonts, int[] colorList, String[] words) {
        this.description = description;
        this.fonts = fonts;
        this.colorList = colorList;
        this.words = words;
        this.fullTextFont = fullTextFont;
    }

    public CoinInfoModel(String description, String[] fonts, int[] colorList, String[] words) {
        this.description = description;
        this.fonts = fonts;
        this.colorList = colorList;
        this.words = words;
    }

    public CoinInfoModel(String description, String fullTextFont, int[] colorList, String[] words) {
        this.description = description;
        this.fullTextFont = fullTextFont;
        this.colorList = colorList;
        this.words = words;
    }

    public CoinInfoModel(String description, int[] colorList, String[] words) {
        this.description = description;
        this.colorList = colorList;
        this.words = words;
    }
}
