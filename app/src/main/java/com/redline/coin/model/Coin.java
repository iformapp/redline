package com.redline.coin.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Coin extends GeneralModel {

    @Expose
    @SerializedName("data")
    public List<Data> data;

    public static class Data {
        @Expose
        @SerializedName("update_value")
        public String updateValue;
        @Expose
        @SerializedName("last_update")
        public String lastUpdate;
        @Expose
        @SerializedName("recommendation")
        public String recommendation;
        @Expose
        @SerializedName("chart_url")
        public String chartUrl;
        @Expose
        @SerializedName("values")
        public String values;
        @Expose
        @SerializedName("status")
        public String status;
        @Expose
        @SerializedName("d_timestamp")
        public String dTimestamp;
        @Expose
        @SerializedName("status_buyornot")
        public String statusBuyornot;
        @Expose
        @SerializedName("loc")
        public String loc;
        @Expose
        @SerializedName("curr")
        public String curr;
        @Expose
        @SerializedName("rank_long")
        public String rankLong;
        @Expose
        @SerializedName("rank_short")
        public String rankShort;
        @Expose
        @SerializedName("img_url")
        public String imgUrl;
        @Expose
        @SerializedName("name")
        public String name;
        @Expose
        @SerializedName("coin_id")
        public String coinId;
        @Expose
        @SerializedName("web_id")
        public String webId;
        @Expose
        @SerializedName("id")
        public String id;

        public String srNo;
    }
}
